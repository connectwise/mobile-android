package com.connectwise.android.ui.contacts;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.connectwise.android.Config;
import com.connectwise.android.R;
import com.connectwise.android.data.loaders.AsyncTaskLoaderResult;
import com.connectwise.android.model.RecordId;
import com.connectwise.android.model.companies.CompanyId;
import com.connectwise.android.model.contacts.Contact;
import com.connectwise.android.ui.LoadingListFragment;
import com.connectwise.android.ui.common.ErrorDialog;
import com.connectwise.android.utils.LogUtils;

import java.util.List;

/**
 *
 */
public class ContactListFragment extends LoadingListFragment implements LoaderManager.LoaderCallbacks<AsyncTaskLoaderResult<List<Contact>>> {

    private static final String TAG = "ContactListFragment";
    private RecordId mRecordId = null;

    public static final int LOADER_ID = 102;

    /**
     * Method to set the content view and initialize long running tasks.
     * </p>
     * <p>
     * This method should be used to instantiate the content view as well as
     * create adapters and loaders that should persist beyond Activity pause operations.
     * </p>
     * This method must be implemented in extension classes.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ContactListAdapter(getActivity());
    }

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common_list, null);
        mLoadingView = view.findViewById(R.id.loading_spinner);
        if (savedInstanceState != null) {
            mRecordId = savedInstanceState.getParcelable(Config.KEY_RECORD_ID);
        } else {
            Bundle bundle = getArguments();
            mRecordId = bundle.getParcelable(Config.KEY_RECORD_ID);
        }
        return view;
    }

    /**
     * Instantiates the list view.
     *
     * @param savedInstanceState bundle containing saved state from paused or stopped instances.
     * @throws IllegalStateException when all component views are not inflated by extension classes.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContentView.setVisibility(View.GONE);
        mRecordId = (CompanyId) (savedInstanceState != null ? savedInstanceState.getParcelable(Config.KEY_RECORD_ID) : getArguments().getParcelable(Config.KEY_RECORD_ID));
        Bundle bundle = new Bundle();
        bundle.putParcelable(ContactSearchLoader.CRITERIA_COMPANY_RECID, mRecordId);
        getLoaderManager().initLoader(LOADER_ID, bundle, this);
    }

    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created.
     * @param args Any arguments supplied by the caller.
     * @return Return a new Loader instance that is ready to start loading.
     */
    @Override
    public Loader<AsyncTaskLoaderResult<List<Contact>>> onCreateLoader(int id, Bundle args) {
        return new ContactSearchLoader(getActivity(), args);
    }

    /**
     * Called when a previously created loader has finished its load.  Note
     * that normally an application is <em>not</em> allowed to commit fragment
     * transactions while in this call, since it can happen after an
     * activity's state is saved.  See {@link android.app.FragmentManager#beginTransaction()
     * FragmentManager.openTransaction()} for further discussion on this.
     * <p/>
     * <p>This function is guaranteed to be called prior to the release of
     * the last data that was supplied for this Loader.  At this point
     * you should remove all use of the old data (since it will be released
     * soon), but should not do your own release of the data since its Loader
     * owns it and will take care of that.  The Loader will take care of
     * management of its data so you don't have to.  In particular:
     * <p/>
     * <ul>
     * <li> <p>The Loader will monitor for changes to the data, and report
     * them to you through new calls here.  You should not monitor the
     * data yourself.  For example, if the data is a {@link android.database.Cursor}
     * and you place it in a {@link android.widget.CursorAdapter}, use
     * the {@link android.widget.CursorAdapter#CursorAdapter(android.content.Context,
     * android.database.Cursor, int)} constructor <em>without</em> passing
     * in either {@link android.widget.CursorAdapter#FLAG_AUTO_REQUERY}
     * or {@link android.widget.CursorAdapter#FLAG_REGISTER_CONTENT_OBSERVER}
     * (that is, use 0 for the flags argument).  This prevents the CursorAdapter
     * from doing its own observing of the Cursor, which is not needed since
     * when a change happens you will get a new Cursor throw another call
     * here.
     * <li> The Loader will release the data once it knows the application
     * is no longer using it.  For example, if the data is
     * a {@link android.database.Cursor} from a {@link android.content.CursorLoader},
     * you should not call close() on it yourself.  If the Cursor is being placed in a
     * {@link android.widget.CursorAdapter}, you should use the
     * {@link android.widget.CursorAdapter#swapCursor(android.database.Cursor)}
     * method so that the old Cursor is not closed.
     * </ul>
     *
     * @param loader The Loader that has finished.
     * @param data   The data generated by the Loader.
     */
    @Override
    public void onLoadFinished(Loader<AsyncTaskLoaderResult<List<Contact>>> loader, AsyncTaskLoaderResult<List<Contact>> data) {
        // Set the new data in the adapter.
        if (data.isError()) {
            LogUtils.LOGE(TAG, data.exception.getMessage(), data.exception);
            new ErrorDialog(this.getActivity(), getString(R.string.error_dialog_title), data.exception.getMessage(), data.exception.getCause());
        } else {
            mAdapter.setData(data.result);
        }
        fadeContentIn();

        // The list should now be shown.
        if (isResumed()) {
            // setListShown(true);
        } else {
            // setListShownNoAnimation(true);
        }
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<AsyncTaskLoaderResult<List<Contact>>> loader) {
        mAdapter.setData(null);
    }

    /**
     *
     */
    @Override
    public void load() {
        super.load();
        Bundle bundle = new Bundle();
        if (mRecordId.getType() == RecordId.COMPANY) {
            bundle.putParcelable(ContactSearchLoader.CRITERIA_COMPANY_RECID, mRecordId);
        } else {
            throw new IllegalStateException(getString(R.string.error_data_unsupported_recordid));
        }
        getLoaderManager().restartLoader(LOADER_ID, bundle, this);
    }

}
