package com.connectwise.android.ui.navigation;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectwise.android.R;
import com.connectwise.android.ui.BaseActivity;
import com.connectwise.android.ui.init.InitialSetupActivity;
import com.connectwise.android.ui.settings.SettingsActivity;
import com.connectwise.android.utils.DateUtils;
import com.connectwise.android.utils.DialogUtils;
import com.connectwise.android.utils.PrefUtils;

/**
 *
 */
public abstract class BaseNavigationActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    protected static final String NAVIGATION_POSITION = "NAVIGATION_POSITION";
    protected int mPosition;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    protected NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #showLocalContextActionBar()}.
     */
    protected CharSequence mTitle;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPosition = extras.getInt(NAVIGATION_POSITION);
        }

        setContentView(R.layout.activity_base_navigation);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    /**
     * @param position
     */
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (mNavigationDrawerFragment != null) {
            NavigationDrawerAdapter adapter = mNavigationDrawerFragment.getAdapter();
            String activityName = adapter.getNavigationActivity(position);
            if (!activityName.equalsIgnoreCase(this.getClass().getCanonicalName())) {
                Class<?> clazz = null;
                if (activityName != null) {
                    try {
                        clazz = Class.forName(activityName);
                    } catch (ClassNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                startActivity(new Intent(this, clazz)
                        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION |
                                Intent.FLAG_ACTIVITY_REORDER_TO_FRONT |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .putExtra(NAVIGATION_POSITION, position));
                finish();
            }
        }
    }

    /**
     * Handles global and common menu options.
     *
     * @param item selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_logout:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.logout_title)
                        .setMessage(R.string.logout_message)
                        .setCancelable(true)
                        .setPositiveButton(R.string.action_confirm, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                PrefUtils.deleteCredential(getApplicationContext());
                                // TODO delete database
                                startActivity(new Intent(getApplicationContext(), InitialSetupActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION |
                                                Intent.FLAG_ACTIVITY_REORDER_TO_FRONT |
                                                Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                finish();
                            }
                        })
                        .setNegativeButton(R.string.action_cancel, null)
                        .show();
                return true;
            case R.id.action_about:
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.activity_about, null);
                TextView versionText = (TextView) view.findViewById(R.id.about_version_text);
                versionText.setText(String.format(getString(R.string.about_version), getVersionName()));

                TextView copyrightText = (TextView) view.findViewById(R.id.about_copyright_text);
                copyrightText.setText(String.format(getString(R.string.about_message), DateUtils.getCurrentYear()));
                new AlertDialog.Builder(this)
                        .setView(view)
                        .setCancelable(true)
                        .setPositiveButton(R.string.action_ok, null)
                        .show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *
     */
    protected void showLocalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        relayoutActionBar();
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    /**
     *
     */
    protected void relayoutActionBar() {
        ImageView imageView = (ImageView) findViewById(android.R.id.home);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.leftMargin = DialogUtils.getDensityPixelValue(this, 8);  // TODO put this in values.
        layoutParams.rightMargin = DialogUtils.getDensityPixelValue(this, 8);
        imageView.setLayoutParams(layoutParams);

    }


}
