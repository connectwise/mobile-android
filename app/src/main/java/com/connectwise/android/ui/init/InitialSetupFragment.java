package com.connectwise.android.ui.init;

import android.app.Activity;
import android.app.Fragment;

import com.connectwise.android.ui.FragmentCallbackListener;

/**
 * Class abstraction to consolidate common code from fragments in the InitialSetupActivity
 */
public abstract class InitialSetupFragment extends Fragment {

    private static String TAG = null;

    // Keys used to interact with various setup fragments.
    public static final String SUCCESS_KEY = "SUCCESS_KEY";
    public static final String FRAGMENT_KEY = "FRAGMENT_KEY";
    public static final int EULA_FRAGMENT = 0;
    public static final int LOGIN_FRAGMENT = 1;
    public static final int INIT_SYNC_FRAGMENT = 2;

    protected FragmentCallbackListener listener;

    /**
     * On attach keep a reference to the calling activity to allow event callbacks to be performed.
     *
     * @param activity calling activity, must implement FragmentCallbackListener
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FragmentCallbackListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement FragmentCallbackListener");
        }
    }

}
