package com.connectwise.android.ui.companies;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.connectwise.android.Config;
import com.connectwise.android.R;
import com.connectwise.android.model.RecordId;
import com.connectwise.android.ui.LoadingListFragment;
import com.connectwise.android.ui.navigation.BaseNavigationActivity;
import com.connectwise.android.utils.DialogUtils;

/**
 *
 */
public class CompanyActivity extends BaseNavigationActivity implements ActionBar.OnNavigationListener, LoadingListFragment.OnSelectedListener {

    /**
     * @param i
     * @param l
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        return false;
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            return;
        }

        // Extensions of BaseNavigationActivity all share the same layout, only switching content fragments.
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CompanySearchFragment fragment = new CompanySearchFragment();
        fragmentTransaction.add(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Called by fragment when a list item is selected
     *
     * @param position
     * @param recordId
     */
    @Override
    public void onSelected(int position, RecordId recordId, String companyName) {

        // IF multipane
//        CompanyDetailFragment fragment = new CompanyDetailFragment();
//        Bundle bundle = new Bundle();
//        bundle.putLong(CompanyDetailFragment.ARG_RECORD_ID, recordId);
//        fragment.setArguments(bundle);
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.container, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

        startActivity(new Intent(this, CompanyDetailActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                .putExtra(Config.KEY_RECORD_ID, recordId));
    }


    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.search, menu);
            inflater.inflate(R.menu.company, menu);
            inflater.inflate(R.menu.global, menu);
            showLocalContextActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * @param item selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_search) {
            CompanySearchFragment fragment = (CompanySearchFragment) getFragmentManager().findFragmentById(R.id.container);
            DialogUtils.hideKeyboard(fragment.getView());
            fragment.refreshView();
            fragment.load();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}