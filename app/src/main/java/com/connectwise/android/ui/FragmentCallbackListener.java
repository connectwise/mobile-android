package com.connectwise.android.ui;

import android.os.Bundle;

/**
 * Interface describing the mechanism for producing callbacks from fragments to their calling Activity.
 */
public interface FragmentCallbackListener {

    /**
     * Method to be called when a fragment needs to notify the calling Activity of an event.
     *
     * @param bundle data regarding the event being passed.
     */
    public void onFragmentCallback(Bundle bundle);

}
