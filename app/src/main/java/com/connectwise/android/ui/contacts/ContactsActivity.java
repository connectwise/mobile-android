package com.connectwise.android.ui.contacts;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.connectwise.android.R;
import com.connectwise.android.model.RecordId;
import com.connectwise.android.ui.LoadingListFragment;
import com.connectwise.android.ui.navigation.BaseNavigationActivity;
import com.connectwise.android.utils.DialogUtils;

/**
 *
 */
public class ContactsActivity extends BaseNavigationActivity implements ActionBar.OnNavigationListener, LoadingListFragment.OnSelectedListener {

    /**
     * @param i
     * @param l
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        return false;
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            return;
        }

        // Extensions of BaseNavigationActivity all share the same layout, only switching content fragments.
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ContactSearchFragment fragment = new ContactSearchFragment();
        fragmentTransaction.add(R.id.container, fragment);
        fragmentTransaction.commit();

    }

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.search, menu);
            inflater.inflate(R.menu.contact, menu);
            inflater.inflate(R.menu.global, menu);
            showLocalContextActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * @param item selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_search) {
            ContactSearchFragment fragment = (ContactSearchFragment) getFragmentManager().findFragmentById(R.id.container);
            DialogUtils.hideKeyboard(fragment.getView());
            fragment.load();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called by fragment when a list item is selected
     *
     * @param position
     * @param recordId
     */
    @Override
    public void onSelected(int position, RecordId recordId, String companyName) {

        // TODO Launch Intent

    }

}