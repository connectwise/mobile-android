package com.connectwise.android.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.connectwise.android.R;
import com.connectwise.android.model.RecordId;

/**
 *
 */
public abstract class LoadingListFragment extends ListFragment {

    protected View mContentView;
    protected View mLoadingView;
    // Data adapter to fill list.
    public BaseListAdapter mAdapter;
    // Variables for dealing with animating the content and progress loading transition.
    protected int mShortAnimationDuration;
    OnSelectedListener mCallback;

    /**
     * Method to set the content view and initialize long running tasks.
     * </p>
     * <p>
     * This method should be used to instantiate the content view as well as
     * create adapters and loaders that should persist beyond Activity pause operations.
     * </p>
     * This method must be implemented in extension classes.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Instantiates the list view.
     *
     * @param savedInstanceState bundle containing saved state from paused or stopped instances.
     * @throws IllegalStateException when all component views are not inflated by extension classes.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mLoadingView == null) {
            throw new IllegalStateException(getString(R.string.error_ui_loadinglist_missingview));
        }
        if (mAdapter == null) {
            throw new IllegalStateException(getString(R.string.error_ui_loadinglist_missingadapter));
        }

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mContentView = getListView();
        setListAdapter(mAdapter);
    }

    /**
     * Abstract method for extension classes to perform search and data access operations.
     */
    public void load() {
        mAdapter.setData(null);
        fadeContentOut();
    }

    /**
     * @param bundle
     * @param key
     * @param value
     */
    protected void addToBundle(Bundle bundle, String key, String value) {
        if (value != null && !TextUtils.isEmpty(value)) {
            bundle.putString(key, value);
        }
    }

    /**
     * @param bundle
     * @param editText
     * @param key
     */
    protected void addToBundle(Bundle bundle, String key, EditText editText) {
        if (editText != null) {
            addToBundle(bundle, key, editText.getText().toString());
        }
    }

    /**
     * Attaches the Fragment callback to the container Activity.
     *
     * @param activity
     * @throws IllegalStateException when the container activity does not implement the OnSelectedListener interface.
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new IllegalStateException(String.format(getString(R.string.error_ui_loadinglist_missinginterface), activity.toString()));
        }
    }

    /**
     * Animate the fade in of the content view while fading out the progress view.
     */
    protected void fadeContentIn() {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mContentView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mLoadingView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoadingView.setVisibility(View.GONE);
                    }
                });
    }

    /**
     * @param listView
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        mCallback.onSelected(position, mAdapter.getItemObject(position).getRecordId(), mAdapter.getItemObject(position).toString());
    }

    /**
     * Animate the fade in of the progress view while fading out the content view.
     */
    protected void fadeContentOut() {
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mLoadingView.setAlpha(0f);
        mLoadingView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mLoadingView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        mContentView.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mContentView.setVisibility(View.GONE);
                    }
                });
    }

    /**
     * Event listener interface for selected list items.
     */
    public interface OnSelectedListener {
        /**
         * Called by fragment when a list item is selected.
         *
         * @param position the position in the list that the selected item occupies.
         * @param recordId record identifier for the Model object selected.
         * @param value    string representation of the Model object.
         */
        public void onSelected(int position, RecordId recordId, String value);
    }

}
