package com.connectwise.android.ui.navigation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.connectwise.android.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class NavigationDrawerAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final List<NavigationObject> mData = new ArrayList<>();

    /**
     * @param context
     * @param data
     */
    public NavigationDrawerAdapter(Context context, NavigationObject[] data) {
        super();
        mData.addAll(new ArrayList<>(Arrays.asList(data)));
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * @param position
     * @return
     */
    public String getNavigationActivity(final int position) {
        NavigationObject navObject = (NavigationObject) getItem(position);
        if (navObject.getObjectType() == NavigationObject.OBJECT_TYPE_ITEM) {
            return ((NavigationItem) navObject).activityClass;
        }
        return null;
    }

    /**
     * @return
     */
    @Override
    public int getCount() {
        return mData.size();
    }

    /**
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * @param position
     * @return
     */
    public NavigationObject getItemObject(int position) {
        return (NavigationObject) getItem(position);
    }

    /**
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return getItemObject(position).getObjectType();
    }

    /**
     * @return
     */
    @Override
    public int getViewTypeCount() {
        return NavigationObject.OBJECT_TYPE_COUNT;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewData viewData;
        NavigationObject navObject = getItemObject(position);
        boolean isItem = navObject.getObjectType() == NavigationObject.OBJECT_TYPE_ITEM;
        if (convertView == null) {
            convertView = mInflater.inflate(navObject.objectLayoutId, parent, false);
            if (isItem) {
                viewData = new ViewItemData();
                viewData.textView = (TextView) convertView.findViewById(R.id.itemText);
                ((ViewItemData) viewData).imageView = (ImageView) convertView.findViewById(R.id.itemIcon);
            } else {
                viewData = new ViewData();
                viewData.textView = (TextView) convertView.findViewById(R.id.itemText);
            }
            convertView.setTag(viewData);
        } else {
            viewData = (ViewData) convertView.getTag();
        }

        viewData.textView.setText(navObject.text);
        if (isItem) {
            ((ViewItemData) viewData).imageView.setImageResource(((NavigationItem) navObject).iconId);
        }

        return convertView;

    }

    /**
     * @return
     */
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    /**
     * @param position
     * @return
     */
    @Override
    public boolean isEnabled(int position) {
        return mData.get(position).getObjectType() == NavigationObject.OBJECT_TYPE_ITEM;
    }

    /**
     *
     */
    private static class ViewData {
        TextView textView;
    }

    /**
     *
     */
    private static class ViewItemData extends ViewData {
        ImageView imageView;
    }

}
