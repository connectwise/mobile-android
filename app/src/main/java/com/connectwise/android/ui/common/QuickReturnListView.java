package com.connectwise.android.ui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

/**
 * Custom ListView designed to contain a quick return header.
 */
public class QuickReturnListView extends ListView {

    private int mItemCount;
    private int mItemOffsetY[];
    private boolean mScrollIsComputed = false;
    private int mHeight;

    /**
     * Default constructor that calls super constructor.
     *
     * @param context application context.
     */
    public QuickReturnListView(Context context) {
        super(context);
    }

    /**
     * Default constructor that calls super constructor.
     *
     * @param context application context.
     * @param attrs   attribute collection.
     */
    public QuickReturnListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Tracks the overall height of the ListView.
     *
     * @return integer value height.
     */
    public int getListHeight() {
        return mHeight;
    }

    /**
     * Compute the ScrollView Y value based on the measurements of the ListView
     */
    public void computeScrollY() {
        mHeight = 0;
        mItemCount = getAdapter().getCount();
        if (mItemOffsetY == null || mItemOffsetY.length != mItemCount) {
            mItemOffsetY = new int[mItemCount];
        }
        int dividerHeight = getDividerHeight();
        for (int i = 0; i < mItemCount; ++i) {
            View view = getAdapter().getView(i, null, this);
            view.measure(
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            mItemOffsetY[i] = mHeight;
            mHeight += view.getMeasuredHeight() + dividerHeight;
        }
        mScrollIsComputed = true;
    }

    /**
     * Indicates whether the ScrollView Y value has been conputed yet.
     *
     * @return True if already computed, else false.
     */
    public boolean scrollYIsComputed() {
        return mScrollIsComputed;
    }

    /**
     * Returns the computed ScrollView Y value.
     *
     * @return integer Y value.
     */
    public int getComputedScrollY() {
        int pos, nScrollY, nItemY;
        View view = null;
        pos = getFirstVisiblePosition();
        view = getChildAt(0);
        nItemY = view.getTop();
        nScrollY = mItemOffsetY[pos] - nItemY;
        return nScrollY;
    }

}