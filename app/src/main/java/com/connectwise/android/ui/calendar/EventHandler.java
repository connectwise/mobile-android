package com.connectwise.android.ui.calendar;

/**
 *
 */
public interface EventHandler {

    long getSupportedEventTypes();

    //   void handleEvent(EventInfo event);

    void eventsChanged();
}
