package com.connectwise.android.ui.navigation;

/**
 *
 */
public class NavigationHeader extends NavigationObject {

    /**
     * @param text
     * @param objectLayoutId
     */
    public NavigationHeader(String text, int objectLayoutId) {
        super(text, objectLayoutId);
    }

    /**
     * @return
     */
    @Override
    public int getObjectType() {
        return OBJECT_TYPE_HEADER;
    }

}
