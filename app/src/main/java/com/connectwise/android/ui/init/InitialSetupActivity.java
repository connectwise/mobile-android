package com.connectwise.android.ui.init;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.connectwise.android.R;
import com.connectwise.android.ui.BaseActivity;
import com.connectwise.android.ui.FragmentCallbackListener;
import com.connectwise.android.ui.dashboard.DashboardActivity;
import com.connectwise.android.utils.PrefUtils;

import static com.connectwise.android.ui.init.InitialSetupFragment.EULA_FRAGMENT;
import static com.connectwise.android.ui.init.InitialSetupFragment.FRAGMENT_KEY;
import static com.connectwise.android.ui.init.InitialSetupFragment.INIT_SYNC_FRAGMENT;
import static com.connectwise.android.ui.init.InitialSetupFragment.LOGIN_FRAGMENT;
import static com.connectwise.android.ui.init.InitialSetupFragment.SUCCESS_KEY;
import static com.connectwise.android.utils.LogUtils.LOGE;
import static com.connectwise.android.utils.LogUtils.getTag;

/**
 * Main entry point for the application.
 * Performs initial setup checks before delegating control to the primary navigation elemtne
 */
public class InitialSetupActivity extends BaseActivity implements FragmentCallbackListener {

    private static final String TAG = getTag("InitialSetupActivity");

    /**
     * Functionality to setup the main view or recreate it from a saved view.
     *
     * @param savedInstanceState saved state from a previous execution that was paused.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            // Set version name on setup screen.
            String versionName = "";
            versionName = "v." + getVersionName();
            TextView versionText = (TextView) findViewById(R.id.main_version_text);
            versionText.setText(versionName);
            // Handle initial fragment creation.
            handleFragment();
        }
    }

    /**
     * Internal method to handle displaying fragments if startup criteria are not met.
     */
    private void handleFragment() {
        if (!PrefUtils.isSetupComplete(getApplicationContext())) {
            if (!PrefUtils.isStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_EULA)) {
                // EULA has not yet been accepted.
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_framecontainer, new InitialEulaFragment())
                        .commit();
            } else if (!PrefUtils.isStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_CRED)) {
                // Authentication credentials have not been validated
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_framecontainer, new InitialLoginFragment())
                        .commit();
            } else if (!PrefUtils.isStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_SYNC)) {
                // Initial sync not completed
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_framecontainer, new InitialSyncFragment())
                        .commit();
            } else {
                // All setup stages are complete, mark setup finished.
                PrefUtils.markSetupComplete(getApplicationContext(), true);
                handleFragment();
            }
        } else {
            Intent intent = new Intent(this, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Functionality to handle callbacks from the main fragments.
     *
     * @param bundle relevant fragment data.
     */
    @Override
    public void onFragmentCallback(Bundle bundle) {
        int fragment = bundle.getInt(FRAGMENT_KEY);
        switch (fragment) {
            case EULA_FRAGMENT:
                // Success in this case indicates the user accepted the EULA.
                if (bundle.getBoolean(SUCCESS_KEY)) {
                    PrefUtils.markStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_EULA, true);
                } else {
                    // Declining the EULA results in the application exiting.
                    finish();
                }
                break;
            case LOGIN_FRAGMENT:
                // Technically success should never be false since it should not exit the fragment with invalid data.
                if (bundle.getBoolean(SUCCESS_KEY)) {
                    PrefUtils.markStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_CRED, true);
                }
                // A non-successful condition will cause the fragment to be displayed again.
                break;
            case INIT_SYNC_FRAGMENT:
                // Success in this case indicates the initial data sync is complete.
                if (bundle.getBoolean(SUCCESS_KEY)) {
                    PrefUtils.markStageComplete(getApplicationContext(), PrefUtils.PREF_SETUP_STAGE_SYNC, true);
                }
                // A non-successful condition will cause the fragment to be displayed again.
                break;
            default:
                LOGE(TAG, "This state should never occur unless development incorrectly configures a fragment.");
                break;
        }
        // Reevaluate UI view after callback.
        handleFragment();

    }

}
