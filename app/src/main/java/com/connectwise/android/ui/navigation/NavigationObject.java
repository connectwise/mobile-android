package com.connectwise.android.ui.navigation;

/**
 *
 */
public abstract class NavigationObject {

    public static final int OBJECT_TYPE_COUNT = 2;
    public static final int OBJECT_TYPE_HEADER = 0;
    public static final int OBJECT_TYPE_ITEM = 1;

    public int objectLayoutId;
    public String text;

    /**
     * @param text
     */
    public NavigationObject(String text, int objectLayoutId) {
        this.text = text;
        this.objectLayoutId = objectLayoutId;
    }

    /**
     * @return
     */
    public abstract int getObjectType();

}
