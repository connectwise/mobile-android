package com.connectwise.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.ListView;

import com.connectwise.android.R;
import com.connectwise.android.ui.common.QuickReturnListView;

/**
 * Base class to abstract common quick return list functionality.
 */
public abstract class QuickReturnListFragment extends LoadingListFragment {

    protected static String TAG = "QuickReturnListFragment";
    // State control for quick return position.
    private static final int STATE_ONSCREEN = 0;
    private static final int STATE_OFFSCREEN = 1;
    private static final int STATE_RETURNING = 2;
    private static final int STATE_EXPANDED = 3;
    private int mState = STATE_ONSCREEN;
    // Views that are part of the composite that is a quick return list.
    protected View mHeader;
    protected View mQuickReturnView;
    protected View mPlaceHolder;
    // Variables to control quick return scrolling.
    private int mCachedVerticalScrollRange;
    private int mQuickReturnHeight;
    private int mScrollY;
    private int mMinRawY = 0;
    private int mRawY;
    private boolean noAnimation = false;
    // Variables for dealing with animating the content and progress loading transition.
    private TranslateAnimation mAnimation;

    /**
     * Method to set the content view and initialize long running tasks.
     * </p>
     * <p>
     * This method should be used to instantiate the content view as well as
     * create adapters and loaders that should persist beyond Activity pause operations.
     * </p>
     * This method must be implemented in extension classes.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * </p>
     * <p>
     * Although in the base implementation this method is optional, in this case
     * extension classes are required to inflate the five views used in quick return lists.
     * <ul>
     * <li>mContentView</li>
     * <li>mLoadingView</li>
     * <li>mHeaderView</li>
     * <li>mQuickReturnView</li>
     * <li>mPlaceHolderView</li>
     * </ul>
     * </p>
     * This method must be implemented in extension classes.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Instantiates the quick return view and subsequent controlling mechanism.
     *
     * @param savedInstanceState bundle containing saved state from paused or stopped instances.
     * @throws IllegalStateException when all component views are not inflated by extension classes.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Verify all prerequisites are met.
        if (mHeader == null ||
                mQuickReturnView == null ||
                mPlaceHolder == null) {
            throw new IllegalStateException(getString(R.string.error_ui_quickreturn_missingview));
        }

        final QuickReturnListView quickReturnListView = (QuickReturnListView) mContentView;

        // Spinner view is hidden by default as these are search views. // TODO future impl will need to allow for immediate search.
        mLoadingView.setVisibility(View.GONE);

        // Retrieve and cache the system's default "short" animation time.
        mShortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        // Add the header view to the listview. This will act as a placeholder in the composite view for the quick return
        // at the top of the list view.
        mContentView = getListView();
        quickReturnListView.addHeaderView(mHeader);
        setListAdapter(mAdapter);

        // Setup observer and listener for vertical scrolling.
        quickReturnListView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mQuickReturnHeight = mQuickReturnView.getHeight();
                        quickReturnListView.computeScrollY();
                        mCachedVerticalScrollRange = quickReturnListView.getListHeight();
                    }
                }
        );
        quickReturnListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                mScrollY = 0;
                int translationY = 0;

                if (quickReturnListView.scrollYIsComputed()) {
                    mScrollY = quickReturnListView.getComputedScrollY();
                }

                mRawY = mPlaceHolder.getTop() - Math.min(mCachedVerticalScrollRange - mContentView.getHeight(), mScrollY);
                switch (mState) {
                    case STATE_OFFSCREEN:
                        if (mRawY <= mMinRawY) {
                            mMinRawY = mRawY;
                        } else {
                            mState = STATE_RETURNING;
                        }
                        translationY = mRawY;
                        break;
                    case STATE_ONSCREEN:
                        if (mRawY < -mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = mRawY;
                        }
                        translationY = mRawY;
                        break;
                    case STATE_RETURNING:

                        if (translationY > 0) {
                            translationY = 0;
                            mMinRawY = mRawY - mQuickReturnHeight;
                        } else if (mRawY > 0) {
                            mState = STATE_ONSCREEN;
                            translationY = mRawY;
                        } else if (translationY < -mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = mRawY;
                        } else if (mQuickReturnView.getTranslationY() != 0 && !noAnimation) {
                            noAnimation = true;
                            mAnimation = new TranslateAnimation(0, 0, -mQuickReturnHeight, 0);
                            mAnimation.setFillAfter(true);
                            mAnimation.setDuration(250);
                            mQuickReturnView.startAnimation(mAnimation);
                            mAnimation.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    noAnimation = false;
                                    mMinRawY = mRawY;
                                    mState = STATE_EXPANDED;
                                }
                            });
                        }
                        break;
                    case STATE_EXPANDED:
                        if (mRawY < mMinRawY - 2 && !noAnimation) {
                            if (mRawY < 0) {
                                translationY = mRawY;
                                mState = STATE_ONSCREEN;
                            } else {
                                noAnimation = true;
                                mAnimation = new TranslateAnimation(0, 0, 0, -mQuickReturnHeight);
                                mAnimation.setFillAfter(true);
                                mAnimation.setDuration(250);
                                mAnimation.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        noAnimation = false;
                                        mState = STATE_OFFSCREEN;
                                    }
                                });
                                mQuickReturnView.startAnimation(mAnimation);
                            }
                        } else if (translationY > 0) {
                            translationY = 0;
                            mMinRawY = mRawY - mQuickReturnHeight;
                        } else if (mRawY > 0) {
                            mState = STATE_ONSCREEN;
                            translationY = mRawY;
                        } else if (translationY < -mQuickReturnHeight) {
                            mState = STATE_OFFSCREEN;
                            mMinRawY = mRawY;
                        } else {
                            mMinRawY = mRawY;
                        }
                }
                mQuickReturnView.setTranslationY(translationY);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

        });

    }

    /**
     * On state change evalutate the position of the content view relative to the quick return.
     */
    public void refreshView() {
        final QuickReturnListView quickReturnListView = (QuickReturnListView) mContentView;
        quickReturnListView.smoothScrollToPosition(0);
        quickReturnListView.computeScrollY();
        mCachedVerticalScrollRange = quickReturnListView.getListHeight();
    }

    /**
     * This method will be called when an item in the list is selected.
     * Subclasses should override. Subclasses can call
     * getListView().getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param listView The ListView where the click happened
     * @param view     The view that was clicked within the ListView
     * @param position The position of the view in the list
     * @param id       The row id of the item that was clicked
     */
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        // Overrides base click functionality as adding a header, i.e. quickreturn, to a
        // list view increases position by one so position must be shifted down one.
        if (!mAdapter.isEmpty()) {
            mCallback.onSelected(position - 1, mAdapter.getItemObject(position - 1).getRecordId(), mAdapter.getItemObject(position - 1).toString());
        }
    }

}
