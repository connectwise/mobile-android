package com.connectwise.android.ui.calendar;

/**
 * Created by mjtrefethen on 2/4/14.
 */
public interface CalendarViewType {
    final int DETAIL = -1;
    final int CURRENT = 0;
    final int AGENDA = 1;
    final int DAY = 2;
    final int EDIT = 5;
    final int MAX_VALUE = 5;
}