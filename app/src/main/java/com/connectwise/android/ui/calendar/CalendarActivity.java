package com.connectwise.android.ui.calendar;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.connectwise.android.R;
import com.connectwise.android.ui.navigation.BaseNavigationActivity;

/**
 *
 */
public class CalendarActivity extends BaseNavigationActivity implements ActionBar.OnNavigationListener {

    private CalendarSpinnerAdapter actionbarAdapter;
    private ActionBar actionBar;

    /**
     * This method is called whenever a navigation item in your action bar
     * is selected.
     *
     * @param itemPosition Position of the item clicked.
     * @param itemId       ID of the item clicked.
     * @return false as extensions of BaseNavigation Activity should never handle navigation events..
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        return false;
    }

    /**
     * Called when the activity is starting.  This is where most initialization
     * should go: calling {@link #setContentView(int)} to inflate the
     * activity's UI, using {@link #findViewById} to programmatically interact
     * with widgets in the UI, calling
     * {@link #managedQuery(android.net.Uri, String[], String, String[], String)} to retrieve
     * cursors for data being displayed, etc.
     * <p/>
     * <p>You can call {@link #finish} from within this function, in
     * which case onDestroy() will be immediately called without any of the rest
     * of the activity lifecycle ({@link #onStart}, {@link #onResume},
     * {@link #onPause}, etc) executing.
     * <p/>
     * <p><em>Derived classes must call through to the super class's
     * implementation of this method.  If they do not, an exception will be
     * thrown.</em></p>
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in {@link #onSaveInstanceState}.  <b><i>Note: Otherwise it is null.</i></b>
     * @see #onStart
     * @see #onSaveInstanceState
     * @see #onRestoreInstanceState
     * @see #onPostCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createButtonsSpinner(CalendarViewType.AGENDA);
    }

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.calendar, menu);
            inflater.inflate(R.menu.global, menu);
            showLocalContextActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * @param item selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    /**
     * @param viewType
     */
    private void createButtonsSpinner(int viewType) {
        actionbarAdapter = new CalendarSpinnerAdapter(this, viewType);
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(actionbarAdapter, this);
        switch (viewType) {
            case CalendarViewType.AGENDA:
                actionBar.setSelectedNavigationItem(CalendarSpinnerAdapter.BUTTON_INDEX_AGENDA);
                break;
            case CalendarViewType.DAY:
                actionBar.setSelectedNavigationItem(CalendarSpinnerAdapter.BUTTON_INDEX_DAY);
                break;
            default:
                actionBar.setSelectedNavigationItem(CalendarSpinnerAdapter.BUTTON_INDEX_AGENDA);
                break;
        }
        actionbarAdapter.setTime(System.currentTimeMillis());
    }

    /**
     *
     */
    @Override
    public void showLocalContextActionBar() {
        actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(false);
        relayoutActionBar();
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setTitle(mTitle);
    }

}