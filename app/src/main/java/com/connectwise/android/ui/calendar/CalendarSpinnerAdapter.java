package com.connectwise.android.ui.calendar;

import android.content.Context;
import android.os.Handler;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.connectwise.android.R;

import java.util.Formatter;
import java.util.Locale;


/**
 *
 */
public class CalendarSpinnerAdapter extends BaseAdapter {

    private static final String TAG = "CalendarSpinnerAdapter";
    // Variables to store button values
    public static final int BUTTON_INDEX_DAY = 0;
    public static final int BUTTON_INDEX_AGENDA = 1;
    private static final String[] BUTTON_NAMES = {"Day", "Agenda"};
    private static final int BUTTON_VIEW_TYPE = 0;
    static final int VIEW_TYPE_NUM = 1;

    private int currentView;
    // Storage variables for often used objects.
    private final Context context;

    private final LayoutInflater inflater;
    private final StringBuilder stringBuilder;
    private final Formatter formatter;
    // Variables to store the current date and time.
    private long currentTime;
    private String currentTimeZone;
    private long currentDay;
    private Handler midnightHandler;

    /**
     * @param context
     */
    public CalendarSpinnerAdapter(Context context, int viewType) {
        super();
        this.context = context;
        this.midnightHandler = new Handler();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.stringBuilder = new StringBuilder(50);
        this.formatter = new Formatter(this.stringBuilder, Locale.getDefault());
        this.currentView = viewType;
        // Setup initial values and handler to reprocess at midnight.
        getTimeValues(context);
    }

    /**
     * Runnable to execute a date and time refresh in a separate thread.
     */
    private final Runnable updaterThread = new Runnable() {
        @Override
        public void run() {
            getTimeValues(context);
        }
    };

    /**
     * Retrieves the current time and zone for the adapter and also initializes the thread process to rerun at midnight.
     *
     * @param context current activity context.
     */
    public void getTimeValues(Context context) {
        // TODO determine better timezone option
        currentTimeZone = Time.getCurrentTimezone();
        Time time = new Time(currentTimeZone);
        long now = System.currentTimeMillis();
        time.set(now);
        currentDay = Time.getJulianDay(now, time.gmtoff);
        notifyDataSetChanged();
        registerMidnightHandler();
    }

    // Update the date that is displayed on buttons
    // Used when the user selects a new day/week/month to watch
    public void setTime(long time) {
        currentTime = time;
        notifyDataSetChanged();
    }

    /**
     * Registers a callback to run the updater thread at midnight.
     */
    private void registerMidnightHandler() {
        midnightHandler.removeCallbacks(updaterThread);
        // Set the time updater to run at 1 second after midnight
        long now = System.currentTimeMillis();
        Time time = new Time(currentTimeZone);
        time.set(now);
        // Get next run time based on amount of time left in the day, plus one
        long runInMillis = (24 * 3600 - time.hour * 3600 - time.minute * 60 - time.second + 1) * 1000;
        midnightHandler.postDelayed(updaterThread, runInMillis);
    }

    /**
     * Removes the callback handler for the updater thread when the activity is paused.
     */
    public void onPause() {
        midnightHandler.removeCallbacks(updaterThread);
    }

    /**
     * Returns the number of buttons produced by the adapter.
     *
     * @return current count of spinner buttons.
     */
    @Override
    public int getCount() {
        return BUTTON_NAMES.length;
    }

    /**
     * Returns the button item name for a given position.
     *
     * @param i position
     * @return button name.
     */
    @Override
    public Object getItem(int i) {
        if (i < BUTTON_NAMES.length) {
            return BUTTON_NAMES[i];
        }
        return null;
    }

    /**
     * Returns the position of a given button.
     * Since this is a static list it simply returns the requested position.
     *
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * Indicates whether the item ids are stable across changes to the underlying data.
     *
     * @return True if the same id always refers to the same object.
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * @param i
     * @param view
     * @param viewGroup
     * @return
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v;

        // Check if current view can be reycled.
        if (view == null || ((Integer) view.getTag()).intValue() != R.layout.calendar_actionbar_selected) {
            v = inflater.inflate(R.layout.calendar_actionbar_selected, viewGroup, false);
            // Set the tag to make sure you can recycle it when you get it
            // as a convert view
            v.setTag(new Integer(R.layout.calendar_actionbar_selected));
        } else {
            v = view;
        }
        TextView weekDay = (TextView) v.findViewById(R.id.button_weekday);
        TextView date = (TextView) v.findViewById(R.id.button_date);

        weekDay.setText(getDayOfWeek());
        date.setText(getFullDate());

        return v;


    }

    @Override
    public int getItemViewType(int position) {
        return BUTTON_VIEW_TYPE;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_NUM;
    }

    @Override
    public boolean isEmpty() {
        return (BUTTON_NAMES.length == 0);
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        View v = inflater.inflate(R.layout.calendar_actionbar_dropdown, viewGroup, false);
        TextView viewType = (TextView) v.findViewById(R.id.button_view);
        TextView date = (TextView) v.findViewById(R.id.button_date);
        switch (i) {
            case BUTTON_INDEX_DAY:
                viewType.setText(BUTTON_NAMES[i]);
                date.setText(getMonthDayDate());
            case BUTTON_INDEX_AGENDA:
                viewType.setText(BUTTON_NAMES[i]);
                date.setText(getMonthDayDate());
                break;
            default:
                v = view;
                break;
        }
        return v;
    }


    /**
     * Builds a string representing the day of the week
     *
     * @return string day of the week
     */
    private String getDayOfWeek() {

        Time t = new Time(currentTimeZone);
        t.set(currentTime);
        long julianDay = Time.getJulianDay(currentTime, t.gmtoff);
        String dayOfWeek = null;
        stringBuilder.setLength(0);

        if (julianDay == currentDay) {
            dayOfWeek = context.getString(R.string.agenda_today,
                    DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                            DateUtils.FORMAT_SHOW_WEEKDAY, currentTimeZone).toString());
        } else if (julianDay == currentDay - 1) {
            dayOfWeek = context.getString(R.string.agenda_yesterday,
                    DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                            DateUtils.FORMAT_SHOW_WEEKDAY, currentTimeZone).toString());
        } else if (julianDay == currentDay + 1) {
            dayOfWeek = context.getString(R.string.agenda_tomorrow,
                    DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                            DateUtils.FORMAT_SHOW_WEEKDAY, currentTimeZone).toString());
        } else {
            dayOfWeek = DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                    DateUtils.FORMAT_SHOW_WEEKDAY, currentTimeZone).toString();
        }
        return dayOfWeek.toUpperCase();
    }

    // Builds strings with different formats:
    // Full date: Month,day Year
    // Month year
    // Month day
    // Month
    // Week:  month day-day or month day - month day
    private String getFullDate() {
        stringBuilder.setLength(0);
        String date = DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR, currentTimeZone).toString();
        return date;
    }

    private String getMonthDayDate() {
        stringBuilder.setLength(0);
        String date = DateUtils.formatDateRange(context, formatter, currentTime, currentTime,
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NO_YEAR, currentTimeZone).toString();
        return date;
    }


}
