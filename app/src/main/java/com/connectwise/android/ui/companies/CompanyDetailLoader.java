package com.connectwise.android.ui.companies;

import android.content.Context;
import android.os.Bundle;

import com.connectwise.android.R;
import com.connectwise.android.data.ApiCriteria;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.companies.CompanyDetailApiDataSource;
import com.connectwise.android.data.companies.CompanyDetailDataFields;
import com.connectwise.android.data.loaders.AsyncTaskLoaderResult;
import com.connectwise.android.model.RecordId;
import com.connectwise.android.model.companies.CompanyDetail;
import com.connectwise.android.ui.BaseAsyncTaskLoader;
import com.connectwise.android.utils.PrefUtils;

import java.util.List;

/**
 * Performs asynchronous loading of company detail data.
 */
public class CompanyDetailLoader extends BaseAsyncTaskLoader<AsyncTaskLoaderResult<CompanyDetail>> {

    public static final String CRITERIA_COMPANY_RECID = CompanyDetailDataFields.REC_ID;

    /**
     * @param context
     * @param bundle
     */
    public CompanyDetailLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /**
     * Executes data loading in a background thread to generate a list of {@link CompanyDetail} object.
     */
    @Override
    public AsyncTaskLoaderResult<CompanyDetail> loadInBackground() {

        CompanyDetailApiDataSource dataSource = new CompanyDetailApiDataSource(getContext(), PrefUtils.getCredential(getContext()));
        ApiCriteria criteria = new ApiCriteria();
        if (mBundle != null) {
            if (mBundle.containsKey(CRITERIA_COMPANY_RECID)) {
                RecordId recordId = mBundle.getParcelable(CRITERIA_COMPANY_RECID);
                criteria.addNonConditional(CRITERIA_COMPANY_RECID, Long.toString(recordId.id));
            }
        }

        try {
            List<CompanyDetail> list = dataSource.getCompanyDetailList(criteria.getXml());
            if (list.size() == 0) {
                return new AsyncTaskLoaderResult<>(new DataAccessException(getContext().getString(R.string.error_data_notfound)));
            } else {
                // This particular loader call can only return a single CompanyDetail object while the datasource could return a list.
                return new AsyncTaskLoaderResult<>(list.get(0));
            }
        } catch (DataAccessException ex) {
            return new AsyncTaskLoaderResult<>(ex);
        }

    }

}
