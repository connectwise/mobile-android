package com.connectwise.android.ui.companies;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.connectwise.android.R;
import com.connectwise.android.model.addresses.AddressFormatter;
import com.connectwise.android.model.addresses.CompanyAddress;
import com.connectwise.android.model.companies.Company;
import com.connectwise.android.ui.BaseListAdapter;
import com.connectwise.android.utils.DialogUtils;
import com.connectwise.android.utils.IntentUtils;

/**
 * Adapter for providing Company lists to ListView containers.
 */
public class CompanyListAdapter extends BaseListAdapter<Company> {

    private static Drawable mMapImageEnabled;
    private static Drawable mMapImageDisabled;
    private static Drawable mCallImageEnabled;
    private static Drawable mCallImageDisabled;

    /**
     * Default constructor.
     * </p>
     * <p>
     * Provided context is passed to the super constructor contained in
     * BaseListAdapter<T> to populate a store a reference to the view inflater.
     * </p>
     *
     * @param context application context for the adapter lifespan.
     */
    public CompanyListAdapter(Context context) {
        super(context);
        // Build static image list
        mMapImageEnabled = context.getResources().getDrawable(R.drawable.ic_action_map);
        mMapImageDisabled = DialogUtils.convertToGrayScale(context.getResources().getDrawable(R.drawable.ic_action_map));
        mCallImageEnabled = context.getResources().getDrawable(R.drawable.ic_action_call);
        mCallImageDisabled = DialogUtils.convertToGrayScale(context.getResources().getDrawable(R.drawable.ic_action_call));
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        ImageButton mapButton = null;
        ImageButton callButton = null;
        if (convertView == null) {
            // Determine which view should display based on parent view context.
            view = mInflater.inflate(R.layout.company_item_row, parent, false);
            // New views require wiring of click events on map and call image buttons
            mapButton = ((ImageButton) view.findViewById(R.id.company_mapbutton));
            wireIntentImageButtonOnClick(mapButton, Intent.ACTION_VIEW);
            callButton = ((ImageButton) view.findViewById(R.id.company_callbutton));
            wireIntentImageButtonOnClick(callButton, Intent.ACTION_DIAL);
        } else {
            // Reuse existing view.
            view = convertView;
        }

        // Get display data object.
        Company company = getItemObject(position);
        // Display company name.
        ((TextView) view.findViewById(R.id.company_name)).setText(company.name);
        TextView locationTextView = ((TextView) view.findViewById(R.id.company_location));

        if (mapButton == null) {
            mapButton = ((ImageButton) view.findViewById(R.id.company_mapbutton));
        }
        // Every company should have one address but we'll check anyway.
        if (company.addresses.size() > 0) {
            // Find service call only returns one default address, should this ever change then a routine to determine
            // which one is default will have to be added here.
            CompanyAddress address = company.addresses.get(0);
            locationTextView.setText(AddressFormatter.toFormattedString(address, AddressFormatter.CITY_STATE));
            // While we have the address determine map button disposition.
            // Map only if there is at least one street line.
            if (address.streetLines.size() == 0 || TextUtils.isEmpty(address.streetLines.get(0))) {
                disableIntentImageButton(mapButton, mMapImageDisabled);
            } else {
                enableIntentImageButton(mapButton, mMapImageEnabled, String.format(IntentUtils.MAP_URI, Uri.encode(AddressFormatter.toFormattedString(address, AddressFormatter.FULL))));
            }
        } else {
            disableIntentImageButton(mapButton, mMapImageDisabled);
        }

        // Determine phone disposition
        if (callButton == null) {
            callButton = ((ImageButton) view.findViewById(R.id.company_callbutton));
        }
        if (TextUtils.isEmpty(company.phoneNumber)) {
            disableIntentImageButton(callButton, mCallImageDisabled);
        } else {
            enableIntentImageButton(callButton, mCallImageEnabled, String.format(IntentUtils.DIAL_URI, company.phoneNumber));
        }

        ((TextView) view.findViewById(R.id.territoryTextView)).setText(company.territory);
        ((TextView) view.findViewById(R.id.typeTextView)).setText(company.type);
        ((TextView) view.findViewById(R.id.statusTextView)).setText(company.status);

        return view;

    }

}
