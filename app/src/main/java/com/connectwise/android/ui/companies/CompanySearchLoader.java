package com.connectwise.android.ui.companies;

import android.content.Context;
import android.os.Bundle;

import com.connectwise.android.data.ApiCriteria;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.addresses.AddressDataFields;
import com.connectwise.android.data.companies.CompanyApiDataSource;
import com.connectwise.android.data.companies.CompanyDataFields;
import com.connectwise.android.data.loaders.AsyncTaskLoaderResult;
import com.connectwise.android.model.companies.Company;
import com.connectwise.android.ui.BaseAsyncTaskLoader;
import com.connectwise.android.utils.PrefUtils;

import java.util.List;

/**
 * Performs asynchronous loading of company data.
 */
public class CompanySearchLoader extends BaseAsyncTaskLoader<AsyncTaskLoaderResult<List<Company>>> {

    public static final String CRITERIA_NAME = CompanyDataFields.NAME;
    public static final String CRITERIA_TYPE = CompanyDataFields.TYPE;
    public static final String CRITERIA_CITY = AddressDataFields.CITY;
    public static final String CRITERIA_STATE = AddressDataFields.STATE;

    /**
     * @param context
     * @param bundle
     */
    public CompanySearchLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /**
     * Executes data loading in a background thread to generate a list of {@link Company} objects.
     */
    @Override
    public AsyncTaskLoaderResult<List<Company>> loadInBackground() {
        CompanyApiDataSource dataSource = new CompanyApiDataSource(getContext(), PrefUtils.getCredential(getContext()));
        ApiCriteria criteria = new ApiCriteria();
        if (mBundle != null) {
            if (mBundle.containsKey(CRITERIA_NAME)) {
                criteria.addEquality(CompanyDataFields.NAME, mBundle.getString(CRITERIA_NAME));
            }
            if (mBundle.containsKey(CRITERIA_TYPE)) {
                criteria.addEquality(CompanyDataFields.TYPE, mBundle.getString(CRITERIA_TYPE));
            }
            if (mBundle.containsKey(CRITERIA_CITY)) {
                criteria.addEquality(AddressDataFields.CITY, mBundle.getString(CRITERIA_CITY));
            }
            if (mBundle.containsKey(CRITERIA_STATE)) {
                criteria.addEquality(AddressDataFields.STATE, mBundle.getString(CRITERIA_STATE));
            }
        }

        try {
            return new AsyncTaskLoaderResult<>(dataSource.getCompanyList(criteria.getConditionXml()));
        } catch (DataAccessException ex) {
            return new AsyncTaskLoaderResult<>(ex);
        }

    }

}