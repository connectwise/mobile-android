package com.connectwise.android.ui.contacts;

import android.content.Context;
import android.os.Bundle;

import com.connectwise.android.data.ApiCriteria;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.companies.CompanyDataFields;
import com.connectwise.android.data.contacts.ContactApiDataSource;
import com.connectwise.android.data.contacts.ContactDataFields;
import com.connectwise.android.data.loaders.AsyncTaskLoaderResult;
import com.connectwise.android.model.companies.CompanyId;
import com.connectwise.android.model.contacts.Contact;
import com.connectwise.android.ui.BaseAsyncTaskLoader;
import com.connectwise.android.utils.PrefUtils;

import java.util.List;

/**
 *
 */
public class ContactSearchLoader extends BaseAsyncTaskLoader<AsyncTaskLoaderResult<List<Contact>>> {

    public static final String CRITERIA_FIRST_NAME = ContactDataFields.FIRST_NAME;
    public static final String CRITERIA_LAST_NAME = ContactDataFields.LAST_NAME;
    public static final String CRITERIA_COMPANY_NAME = ContactDataFields.COMPANY_NAME;
    public static final String CRITERIA_COMPANY_RECID = CompanyDataFields.REC_ID;

    /**
     * @param context
     * @param bundle
     */
    public ContactSearchLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /**
     * Executes data loading in a background thread to generate a list of {@link Contact} objects.
     */
    @Override
    public AsyncTaskLoaderResult<List<Contact>> loadInBackground() {
        ContactApiDataSource dataSource = new ContactApiDataSource(getContext(), PrefUtils.getCredential(getContext()));
        ApiCriteria criteria = new ApiCriteria();
        if (mBundle != null) {
            if (mBundle.containsKey(CRITERIA_FIRST_NAME)) {
                criteria.addEquality(ContactDataFields.FIRST_NAME, mBundle.getString(CRITERIA_FIRST_NAME));
            }
            if (mBundle.containsKey(CRITERIA_LAST_NAME)) {
                criteria.addEquality(ContactDataFields.LAST_NAME, mBundle.getString(CRITERIA_LAST_NAME));
            }
            if (mBundle.containsKey(CRITERIA_COMPANY_NAME)) {
                criteria.addEquality(ContactDataFields.COMPANY_NAME, mBundle.getString(CRITERIA_COMPANY_NAME));
            }
            if (mBundle.containsKey(CRITERIA_COMPANY_RECID)) {
                CompanyId companyId = mBundle.getParcelable(CRITERIA_COMPANY_RECID);
                criteria.addEquality(CompanyDataFields.REC_ID, companyId.id);
            }
        }

        try {
            return new AsyncTaskLoaderResult<>(dataSource.getContactList(criteria.getConditionXml()));
        } catch (DataAccessException ex) {
            return new AsyncTaskLoaderResult<>(ex);
        }

    }

}
