package com.connectwise.android.ui.init;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.connectwise.android.R;

/**
 * UI Fragment to display the application EULA and prompt the user to accept or decline it.
 */
public class InitialEulaFragment extends InitialSetupFragment {

    /**
     * Prior to display of the fragment buttons on the UI need to have their listeners mapped to methods
     * in the fragment. By default all UI listener events are propagated to the Activity which when hosting mulitple
     * pieces of functionality violates the single purpose principle.
     *
     * @param inflater           the resource inflater.
     * @param container          the ViewGroup container.
     * @param savedInstanceState saved instance state for resumed fragments.
     * @return reference to the View created.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View fragmentView = inflater.inflate(R.layout.fragment_eula, container, false);

        Button acceptButton = (Button) fragmentView.findViewById(R.id.eula_action_accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                onAcceptClicked(fragmentView);
            }
        });

        Button declineButton = (Button) fragmentView.findViewById(R.id.eula_action_decline);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                onDeclineClicked(fragmentView);
            }
        });

        return fragmentView;

    }

    /**
     * Click event for the accept button on the EULA form.
     *
     * @param view View reference
     */
    public void onAcceptClicked(View view) {
        handleExit(true);
    }

    /**
     * Click event for the declien button on the EULA foem.
     *
     * @param view View reference
     */
    public void onDeclineClicked(View view) {
        handleExit(false);
    }

    /**
     * Method to handle returning fragment data to the calling Activity.
     *
     * @param accepted boolean value indicating whether the use accepted or declined the EULA.
     */
    private void handleExit(boolean accepted) {
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_KEY, EULA_FRAGMENT);
        bundle.putBoolean(SUCCESS_KEY, accepted);
        listener.onFragmentCallback(bundle);
    }

}
