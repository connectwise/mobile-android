package com.connectwise.android.ui;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;

/**
 *
 */
public abstract class BaseAsyncTaskLoader<T> extends AsyncTaskLoader<T> {

    protected T mResult;
    protected Bundle mBundle = null;

    /**
     * @param context
     */
    public BaseAsyncTaskLoader(Context context, Bundle bundle) {
        super(context);
        if (bundle != null && bundle.size() > 0) {
            mBundle = bundle;
        }
    }

    public abstract T loadInBackground();

    @Override
    public void deliverResult(T data) {

        // If loader has been reset since load method was called dispose of result.
        if (isReset()) {
            if (data != null) {
                releaseResources(data);
                return;
            }
        }

        T oldList = mResult;
        mResult = data;

        // Deliver data to caller.
        if (isStarted()) {
            super.deliverResult(data);
        }

        // New data has been delivered, dispose of old data set.
        if (oldList != null && oldList != data) {
            releaseResources(oldList);
        }

    }

    /**
     * Callback resulting from a call to {@link #startLoading()}.
     */
    @Override
    protected void onStartLoading() {
        if (mResult != null) {
            deliverResult(mResult);
        }
        if (takeContentChanged()) {
            forceLoad();
        } else if (mResult == null) {
            forceLoad();
        }
    }

    /**
     * Callback resulting from a call to {@link #stopLoading()}.
     */
    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    /**
     * Callback resulting from a call to {@link #reset()}.
     */
    @Override
    protected void onReset() {
        onStopLoading();
        if (mResult != null) {
            releaseResources(mResult);
            mResult = null;
        }
    }

    /**
     * Called if the task was canceled before it was completed.  Gives the class a chance
     * to clean up post-cancellation and to properly dispose of the result.
     *
     * @param data The value that was returned by {@link #loadInBackground}, or null
     *             if the task threw {@link android.os.OperationCanceledException}.
     */
    @Override
    public void onCanceled(T data) {
        super.onCanceled(data);
        releaseResources(data);
    }

    /**
     * Force an asynchronous load. Unlike {@link #startLoading()} this will ignore a previously
     * loaded data set and load a new one.  This simply calls through to the
     * implementation's {@link #onForceLoad()}.  You generally should only call this
     * when the loader is started -- that is, {@link #isStarted()} returns true.
     */
    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    /**
     * Helper method to take care of releasing resources associated with an
     * actively loaded data set.
     */
    private void releaseResources(T data) {
    }

}
