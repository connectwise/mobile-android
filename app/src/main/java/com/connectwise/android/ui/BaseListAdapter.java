package com.connectwise.android.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

import com.connectwise.android.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class BaseListAdapter<T extends Model> extends BaseAdapter {

    protected LayoutInflater mInflater;
    protected final List<T> mData = new ArrayList<>();

    public BaseListAdapter(Context context) {
        super();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Determines if the adapter should discriminate between views returned
     * when in the context of a parent or if the adapter is the parent view.
     * </p>
     * <p>
     * An example of this is the ContactListAdapter where it is possible to
     * retrieve a list of Contact objects through searching and from the context
     * of a Company. In the first case the returned View contains a Company name
     * while in the second they do not. In the second case all the entries would
     * have the same name as they were the result of a Contact search within the
     * context of a Company and the name information is then irrelevant as item
     * row data.
     * </p>
     */
    public boolean isParentView = true;

    /**
     * @param data
     */
    public void setData(List<T> data) {
        mData.clear();
        if (data != null) {
            mData.addAll(data);
        }
        notifyDataSetChanged();
    }

    /**
     * @return
     */
    @Override
    public int getCount() {
        return mData.size();
    }

    /**
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return position < mData.size() ? mData.get(position) : null;
    }

    /**
     * @param position
     * @return
     */
    public T getItemObject(int position) {
        return (T) getItem(position);
    }

    /**
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get the type of View that will be created by {@link #getView} for the specified item.
     *
     * @param position The position of the item within the adapter's data set whose view type we
     *                 want.
     * @return An integer representing the type of View. Two views should share the same type if one
     * can be converted to the other in {@link #getView}. Note: Integers must be in the
     * range 0 to {@link #getViewTypeCount} - 1.
     */
    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    /**
     * <p>
     * Returns the number of types of Views that will be created by
     * {@link #getView}. Each type represents a set of views that can be
     * converted in {@link #getView}. If the adapter always returns the same
     * type of View for all items, this method should return 1.
     * </p>
     * <p>
     * This method will only be called when when the adapter is set on the
     * the {@link android.widget.AdapterView}.
     * </p>
     *
     * @return The number of types of Views that will be created by this adapter
     */
    @Override
    public int getViewTypeCount() {
        return 1;
    }

    /**
     * Populate new items in the list.
     */
    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    /**
     * Wires a listener to the onClick event of the image button with a specified
     * Intent action using the tag data as payload.
     *
     * @param button       ImageButton to wire the event to.
     * @param intentAction Intent action to be fired
     */
    protected void wireIntentImageButtonOnClick(ImageButton button, final String intentAction) {

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(intentAction);
                intent.setData(Uri.parse((String) view.getTag()));
                Context context = view.getContext();
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                } else {
                    // TODO alert dialog
                }
            }
        });

    }

    /**
     * Enables an ImageButton designed to perform an Intent activity based on the tag value.
     *
     * @param button ImageButton to enable.
     * @param image  Drawable to display in the button.
     * @param value  value to set into the tag field.
     */
    protected void enableIntentImageButton(ImageButton button, Drawable image, final String value) {
        button.setEnabled(true);
        button.setTag(value);
        button.setImageDrawable(image);
    }

    /**
     * Disables an ImageButton.
     *
     * @param button ImageButton to enable.
     * @param image  Drawable to display in the button.
     */
    protected void disableIntentImageButton(ImageButton button, Drawable image) {
        button.setEnabled(false);
        button.setImageDrawable(image);
    }

}
