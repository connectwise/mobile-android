package com.connectwise.android.ui.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.connectwise.android.R;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 */
public class ErrorDialog {

    private String mTitle;
    private String mMessage;
    private Context mContext;
    private Throwable mException;

    /**
     * @param context
     * @param title
     * @param message
     */
    public ErrorDialog(Context context, String title, String message) {
        mContext = context;
        mTitle = title;
        mMessage = message;
    }

    /**
     * @param context
     * @param title
     * @param message
     * @param exception
     */
    public ErrorDialog(Context context, String title, String message, Throwable exception) {
        this(context, title, message);
        mException = exception;
    }

    /**
     *
     */
    public void show() {
        if (mException == null) {
            showNoException();
        } else {
            showException();
        }
    }

    /**
     *
     */
    private void showNoException() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    /**
     *
     */
    private void showException() {

        AlertDialog.Builder builder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.error_dialog, null);


        TextView briefTextView = (TextView) layout.findViewById(R.id.brief_content);
        briefTextView.setText(mMessage);
        final TextView labelTextView = (TextView) layout.findViewById(R.id.detail_label);
        final TextView detailTextView = (TextView) layout.findViewById(R.id.detail_content);
        final ImageView labelImageView = (ImageView) layout.findViewById(R.id.detail_image);

        LinearLayout expandLayout = (LinearLayout) layout.findViewById(R.id.error_expand_layout);
        expandLayout.setOnClickListener(new View.OnClickListener() {

            /**
             * Event handler to handle touch of layout.
             *
             * @param view reference to the view that was clicked.
             */
            @Override
            public void onClick(final View view) {
                if (detailTextView.getVisibility() == View.VISIBLE) {
                    detailTextView.setVisibility(View.GONE);
                    labelTextView.setText(mContext.getString(R.string.action_show_detail));
                    labelImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_expand));
                } else {
                    detailTextView.setVisibility(View.VISIBLE);
                    labelTextView.setText(mContext.getString(R.string.action_hide_detail));
                    labelImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_action_collapse));
                }
            }

        });

        detailTextView.setMovementMethod(new ScrollingMovementMethod());
        StringWriter stringWriter = new StringWriter();
        stringWriter.append(mException.getMessage());
        stringWriter.append("\n");
        mException.printStackTrace(new PrintWriter(stringWriter));
        detailTextView.setText(stringWriter.toString());

        builder = new AlertDialog.Builder(mContext);
        builder.setView(layout);
        builder.setTitle(mTitle);
        builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

}