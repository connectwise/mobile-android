package com.connectwise.android.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

import com.connectwise.android.Config;
import com.connectwise.android.model.RecordId;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class BaseTabsPagerAdapter extends FragmentPagerAdapter {

    private RecordId mRecordId;
    private List<FragmentDefinition> mFragments = new ArrayList<>();

    public BaseTabsPagerAdapter(FragmentManager fragmentManager, RecordId recordId) {
        super(fragmentManager);
        mRecordId = recordId;
    }

    /**
     * @param name
     * @param clazz
     */
    public void addItem(String name, Class clazz) {
        mFragments.add(new FragmentDefinition(name, clazz));
    }

    /**
     * @param index
     * @return
     */
    @Override
    public Fragment getItem(int index) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Config.KEY_RECORD_ID, mRecordId);
        FragmentDefinition definition = mFragments.get(index);
        if (definition.fragment == null) {
            Fragment fragment = null;
            try {
                Class<?> clazz = mFragments.get(index).clazz;
                Constructor<?> ctor = clazz.getConstructor();
                fragment = (Fragment) ctor.newInstance();
                fragment.setArguments(bundle);
            } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
            definition.fragment = fragment;
            return definition.fragment;
        } else {
            return definition.fragment;
        }
    }

    /**
     * @param index
     * @return
     */
    public String getItemName(int index) {
        FragmentDefinition definition = mFragments.get(index);
        return definition.name;
    }

    /**
     * @return
     */
    @Override
    public int getCount() {
        return mFragments.size();
    }

    /**
     *
     */
    private class FragmentDefinition {
        String name;
        Class clazz;
        Fragment fragment = null;

        /**
         * @param name
         * @param clazz
         */
        private FragmentDefinition(String name, Class clazz) {
            this.name = name;
            this.clazz = clazz;
        }

    }

}
