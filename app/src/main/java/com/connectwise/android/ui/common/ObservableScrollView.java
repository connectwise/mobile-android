package com.connectwise.android.ui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Custom ScrollView with callbacks for scroll listener events.
 */
public class ObservableScrollView extends ScrollView {

    private Callbacks mCallbacks;

    /**
     * Default constructor that calls super constructor.
     *
     * @param context application context.
     */
    public ObservableScrollView(Context context) {
        super(context);
    }

    /**
     * Default constructor that calls super constructor.
     *
     * @param context application context.
     * @param attrs   attribute collection.
     */
    public ObservableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Handles touch screen motion events.
     *
     * @param event The motion event.
     * @return True if the event was handled, false otherwise.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mCallbacks != null) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    mCallbacks.onDownMotionEvent();
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    mCallbacks.onUpOrCancelMotionEvent();
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    /**
     * This is called in response to an internal scroll in this view (i.e., the
     * view scrolled its own contents). This is typically as a result of
     * {@link #scrollBy(int, int)} or {@link #scrollTo(int, int)} having been
     * called.
     *
     * @param hOrigin    Current horizontal scroll origin.
     * @param vOrigin    Current vertical scroll origin.
     * @param oldHOrigin Previous horizontal scroll origin.
     * @param oldVOrigin Previous vertical scroll origin.
     */
    @Override
    protected void onScrollChanged(int hOrigin, int vOrigin, int oldHOrigin, int oldVOrigin) {
        super.onScrollChanged(hOrigin, vOrigin, oldHOrigin, oldVOrigin);
        if (mCallbacks != null) {
            mCallbacks.onScrollChanged(vOrigin);
        }
    }

    /**
     * <p>The scroll range of a scroll view is the overall height of all of its
     * children.</p>
     */
    @Override
    public int computeVerticalScrollRange() {
        return super.computeVerticalScrollRange();
    }

    /**
     * Attach listener to local callback methods.
     *
     * @param listener callback listener
     */
    public void setCallbacks(Callbacks listener) {
        mCallbacks = listener;
    }

    /**
     * Callback events.
     */
    public static interface Callbacks {
        public void onScrollChanged(int scrollY);

        public void onDownMotionEvent();

        public void onUpOrCancelMotionEvent();
    }

}
