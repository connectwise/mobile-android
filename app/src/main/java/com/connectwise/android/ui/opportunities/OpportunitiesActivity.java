package com.connectwise.android.ui.opportunities;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.connectwise.android.R;
import com.connectwise.android.ui.navigation.BaseNavigationActivity;

/**
 *
 */
public class OpportunitiesActivity extends BaseNavigationActivity implements ActionBar.OnNavigationListener {

    /**
     * @param i
     * @param l
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        return false;
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.global, menu);
            showLocalContextActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * @param item selected menu item.
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

}