package com.connectwise.android.ui.init;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.connectwise.android.R;

/**
 *
 */
public class InitialSyncFragment extends InitialSetupFragment {

    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    private int notificationId = 1;

    /**
     * Prior to display of the fragment UI elements need to have their listeners mapped to methods
     * in the fragment. By default all UI listener events are propagated to the Activity which when hosting multiple
     * pieces of functionality violates the single purpose principle.
     *
     * @param inflater           the resource inflater.
     * @param container          the ViewGroup container.
     * @param savedInstanceState saved instance state for resumed fragments.
     * @return reference to the View created.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View fragmentView = inflater.inflate(R.layout.fragment_initsync, container, false);

        Activity activity = getActivity();
        notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(activity);
        builder.setContentTitle("Data Synchronization")
                .setContentText("Initial data load in progress")
                .setProgress(0, 0, true)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher);

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // Announce success to calling activity
                Bundle bundle = new Bundle();
                bundle.putInt(FRAGMENT_KEY, INIT_SYNC_FRAGMENT);
                bundle.putBoolean(SUCCESS_KEY, true);
                listener.onFragmentCallback(bundle);
            }
        };


        // Start a lengthy operation in a background thread
        new Thread(
                new Runnable() {

                    @Override
                    public void run() {
                        builder.setProgress(0, 0, true);
                        notificationManager.notify(notificationId, builder.build());
                        try {
                            // TODO Actually perform sync from service.
                            // Sleep for 10 seconds
                            Thread.sleep(10 * 1000);
                            handler.handleMessage(null);
                        } catch (InterruptedException e) {
                            notificationManager.cancelAll();
                        }
                        // When the loop is finished, updates the notification
                        builder.setContentText("Download complete");
                        builder.setProgress(0, 0, false);
                        builder.setOngoing(false);
                        notificationManager.notify(notificationId, builder.build());
                    }

                }
        ).start();

        return fragmentView;

    }


}
