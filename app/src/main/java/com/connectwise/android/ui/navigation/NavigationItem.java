package com.connectwise.android.ui.navigation;

/**
 *
 */
public class NavigationItem extends NavigationObject {

    public int iconId;
    public String activityClass;

    /**
     * @param text
     * @param objectLayoutId
     * @param iconId
     * @param activityClass
     */
    public NavigationItem(String text, int objectLayoutId, int iconId, String activityClass) {
        super(text, objectLayoutId);
        this.iconId = iconId;
        this.activityClass = activityClass;
    }

    /**
     * @return
     */
    @Override
    public int getObjectType() {
        return OBJECT_TYPE_ITEM;
    }

}
