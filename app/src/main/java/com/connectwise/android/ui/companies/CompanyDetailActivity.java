package com.connectwise.android.ui.companies;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.connectwise.android.Config;
import com.connectwise.android.R;
import com.connectwise.android.model.RecordId;
import com.connectwise.android.model.companies.CompanyId;
import com.connectwise.android.ui.BaseTabsPagerAdapter;
import com.connectwise.android.ui.LoadingListFragment;
import com.connectwise.android.ui.activities.ActivityListFragment;
import com.connectwise.android.ui.configurations.ConfigurationListFragment;
import com.connectwise.android.ui.contacts.ContactDetailActivity;
import com.connectwise.android.ui.contacts.ContactListFragment;
import com.connectwise.android.ui.opportunities.OpportunityListFragment;
import com.connectwise.android.ui.tickets.TicketListFragment;

/**
 *
 */
public class CompanyDetailActivity extends FragmentActivity implements LoadingListFragment.OnSelectedListener {

    private ViewPager mViewPager;
    private BaseTabsPagerAdapter mPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_detail);

        CompanyId recordId;

        // Show the Up button in the action bar.
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        FragmentManager fragmentManager = getFragmentManager();

        if (savedInstanceState != null) {
            recordId = savedInstanceState.getParcelable(Config.KEY_RECORD_ID);
        } else {
            Bundle bundle = getIntent().getExtras();
            recordId = bundle.getParcelable(Config.KEY_RECORD_ID);
        }

        mPagerAdapter = new BaseTabsPagerAdapter(fragmentManager, recordId);
        mPagerAdapter.addItem(getString(R.string.company_title), CompanyDetailFragment.class);
        mPagerAdapter.addItem(getString(R.string.contact_title_plural), ContactListFragment.class);
        mPagerAdapter.addItem(getString(R.string.opportunity_title_plural), OpportunityListFragment.class);
        mPagerAdapter.addItem(getString(R.string.ticket_title_plural), TicketListFragment.class);
        mPagerAdapter.addItem(getString(R.string.activity_title_plural), ActivityListFragment.class);
        mPagerAdapter.addItem(getString(R.string.configuration_title_plural), ConfigurationListFragment.class);

        actionBar.setTitle(R.string.company_detail_actionbar_title);
        mViewPager = (ViewPager) findViewById(R.id.detail_pager);
        mViewPager.setAdapter(mPagerAdapter);
        /**
         * on swiping the viewpager make respective tab selected
         * */
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                               @Override
                                               public void onPageSelected(int position) {
                                                   // on changing the page select respective tab.
                                                   getActionBar().setSelectedNavigationItem(position);
                                               }

                                               @Override
                                               public void onPageScrolled(int arg0, float arg1, int arg2) {
                                               }

                                               @Override
                                               public void onPageScrollStateChanged(int arg0) {
                                               }
                                           }
        );


        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            actionBar.addTab(actionBar.newTab().setText(mPagerAdapter.getItemName(i)).setTabListener(tabListener));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called by fragment when a list item is selected
     *
     * @param position
     * @param recordId
     */
    public void onSelected(int position, RecordId recordId, String value) {

        // TODO spawn detail correct activity for active fragment.
        // IF multipane
//        CompanyDetailFragment fragment = new CompanyDetailFragment();
//        Bundle bundle = new Bundle();
//        bundle.putLong(CompanyDetailFragment.ARG_RECORD_ID, recordId);
//        fragment.setArguments(bundle);
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.container, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

        startActivity(new Intent(this, ContactDetailActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                .putExtra(Config.KEY_RECORD_ID, recordId));
    }

}
