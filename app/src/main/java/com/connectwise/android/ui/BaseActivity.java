package com.connectwise.android.ui;

import android.app.Activity;
import android.content.pm.PackageManager;

import com.connectwise.android.R;

import static com.connectwise.android.utils.LogUtils.LOGE;

/**
 *
 */
public abstract class BaseActivity extends Activity {

    protected final static String TAG = null;

    /**
     * @return
     * @throws PackageManager.NameNotFoundException
     */
    protected String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException | NullPointerException ex) {
            LOGE(TAG, getText(R.string.error_version_not_found).toString(), ex);
            ex.printStackTrace();
        }
        return "0";
    }

}
