package com.connectwise.android.ui.init;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.connectwise.android.R;
import com.connectwise.android.model.Credential;
import com.connectwise.android.ui.common.ErrorDialog;
import com.connectwise.android.utils.PrefUtils;

import static com.connectwise.android.utils.LogUtils.LOGW;

/**
 * UI Fragment to display the credentials data entry view and test initial authentication.
 */
public class InitialLoginFragment extends InitialSetupFragment {

    private static String TAG = "InitialLoginFragment";

    /**
     * Prior to display of the fragment buttons on the UI need to have their listeners mapped to methods
     * in the fragment. By default all UI listener events are propagated to the Activity which when hosting multiple
     * pieces of functionality violates the single purpose principle.
     *
     * @param inflater           the resource inflater.
     * @param container          the ViewGroup container.
     * @param savedInstanceState saved instance state for resumed fragments.
     * @return reference to the View created.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);

        // On click of SSL row, need to toggle the checkbox and hide the keyboard.
        // This is necessary to simulate selection functionality similar to a CheckedTextView.
        LinearLayout sslLayout = (LinearLayout) fragmentView.findViewById(R.id.login_ssl_layout);
        sslLayout.setOnClickListener(new View.OnClickListener() {

            /**
             * Event handler to handle touch of layout containing the checkbox.
             *
             * @param view reference to the view that was clicked.
             */
            @Override
            public void onClick(final View view) {
                CheckBox sslCheckbox = (CheckBox) view.findViewById(R.id.login_ssl_checkbox);
                // Hide keyboard
                try {
                    InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (NullPointerException ex) {
                    LOGW(TAG, "Could not obtain a reference to the InputMethodManager, unable to automatically hide the soft keyboard.", ex);
                }
                sslCheckbox.toggle();
            }

        });

        // Wire event handler to local method.
        Button resetButton = (Button) fragmentView.findViewById(R.id.login_action_reset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                onResetClick((Activity) view.getContext());
            }
        });

        // Wire event handler to local method.
        Button submitButton = (Button) fragmentView.findViewById(R.id.login_action_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                onSubmitClick((Activity) view.getContext());
            }
        });

        return fragmentView;
    }

    /**
     * Reset all form fields.
     *
     * @param activity reference to the calling activity.
     */
    private void onResetClick(final Activity activity) {
        resetEditText(activity, R.id.login_url_text);
        resetEditText(activity, R.id.login_company_text);
        resetEditText(activity, R.id.login_member_text);
        resetEditText(activity, R.id.login_password_text);
        resetEditText(activity, R.id.login_member_text);
        CheckBox sslCheckbox = (CheckBox) activity.findViewById(R.id.login_ssl_checkbox);
        sslCheckbox.setChecked(false);
    }

    /**
     * Method to reset an individual TextView
     *
     * @param activity reference to the calling view.
     * @param id       identifier of the TextView to reset.
     */
    private void resetEditText(final Activity activity, final int id) {
        EditText editText = (EditText) activity.findViewById(id);
        editText.setText("");
        editText.setError(null);
    }

    /**
     * Validate and test authentication.
     * On success fire event to activity to continue on setup path.
     *
     * @param activity reference to the calling activity.
     */
    private void onSubmitClick(final Activity activity) {
        if (isValidForm(activity)) {
            // TODO Test authentication in parallel task while displaying progress bar.

            boolean authenticated = true;
            if (authenticated) {
                // Save authenticated credential to preferences
                PrefUtils.setCredential(getActivity().getApplicationContext(), getCredential(activity)
                );
                // Announce success to calling activity
                Bundle bundle = new Bundle();
                bundle.putInt(FRAGMENT_KEY, LOGIN_FRAGMENT);
                bundle.putBoolean(SUCCESS_KEY, true);
                listener.onFragmentCallback(bundle);
            } else {
                new ErrorDialog(this.getActivity(), getString(R.string.error_auth_title), getString(R.string.error_auth_fields_incorrect));
            }
        }
    }

    /**
     * For a specific Activity and id, return a CheckBox, isChecked()
     *
     * @param activity reference to an Activity.
     * @param id       identifier of the CheckBox.
     * @return true if CheckBox is checked, else false.
     */
    private boolean getCheckBoxValue(final Activity activity, int id) {
        CheckBox checkBox = (CheckBox) activity.findViewById(id);
        return checkBox.isChecked();
    }

    /**
     * For a specific Activity and id, return an EditText.
     *
     * @param activity reference to an Activity.
     * @param id       identifier of the CheckBox.
     * @return EditText object.
     */
    private EditText getEditText(final Activity activity, int id) {
        return (EditText) activity.findViewById(id);
    }

    /**
     * For a specific Activity and id, return an EditText text string.
     *
     * @param activity reference to an Activity.
     * @param id       identifier of the CheckBox.
     * @return text value from EditText.
     */
    private String getEditTextValue(final Activity activity, final int id) {
        return getEditText(activity, id).getText().toString();
    }

    /**
     * Validate the form to guarantee that required fields are filled.
     *
     * @param activity reference to teh activity containing the form.
     * @return true if all required form fields are filled, else false.
     */
    private boolean isValidForm(final Activity activity) {

        boolean valid = true;

        EditText urlEditText = getEditText(activity, R.id.login_url_text);
        valid = isValidEditText(urlEditText) && valid;
        EditText companyEditText = getEditText(activity, R.id.login_company_text);
        valid = isValidEditText(companyEditText) && valid;
        EditText memberEditText = getEditText(activity, R.id.login_member_text);
        valid = isValidEditText(memberEditText) && valid;
        EditText passwordEditText = getEditText(activity, R.id.login_password_text);
        valid = isValidEditText(passwordEditText) && valid;

        return valid;

    }

    /**
     * Validate that a particular EditText is filled.
     *
     * @param editText
     * @return true if the EditText is filled, else false.
     */
    private boolean isValidEditText(final EditText editText) {

        String text = editText.getText().toString();

        if (TextUtils.isEmpty(text)) {
            editText.setError(getString(R.string.error_required_field));
            return false;
        }
        return true;

    }

    /**
     * @return
     */
    private Credential getCredential(final Activity activity) {
        return new Credential(getEditTextValue(activity, R.id.login_url_text),
                getEditTextValue(activity, R.id.login_company_text),
                getEditTextValue(activity, R.id.login_member_text),
                getEditTextValue(activity, R.id.login_password_text),
                getCheckBoxValue(activity, R.id.login_ssl_checkbox));
    }

}
