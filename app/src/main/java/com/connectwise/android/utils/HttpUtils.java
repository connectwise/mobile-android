package com.connectwise.android.utils;

/**
 * Utility class to encapsulate common HTTP utilities.
 */
public class HttpUtils {

    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    /**
     * Common encoding values.
     */
    public class Encoding {
        public static final String UTF_8 = "UTF-8";
    }

}
