package com.connectwise.android.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;

/**
 * Common functionality for interacting with networking.
 */
public class NetworkUtils {

    /**
     * Determines the disposition of the Airplane Mode.
     *
     * @param context application context to use.
     * @return true if enabled, false otherwise.
     */
    public static boolean isAirplaneModeOn(Context context) {
        // Android changed this mechanism in 4.2
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    /**
     * Determines is the device has internet connectivity.
     *
     * @param context application context to use.
     * @return true if connected, false otherwise.
     */
    public static boolean isConnected(final Context context) {
        return isConnected(getNetworkInfo(context));
    }

    /**
     * Determines if the device has internet connectivity specifically through WiFi.
     *
     * @param context application context to use.
     * @return true if connected, false otherwise.
     */
    public static boolean isWiFiConnected(final Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        return isConnected(networkInfo) && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * Retrieve network information from Context.
     *
     * @param context application context to use.
     * @return current network information.
     */
    private static NetworkInfo getNetworkInfo(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Determine basic connectivity.
     *
     * @param networkInfo already retrieved network information.
     * @return true if connected, false otherwise.
     */
    private static boolean isConnected(NetworkInfo networkInfo) {
        return networkInfo != null && networkInfo.isConnected();
    }

}
