package com.connectwise.android.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.connectwise.android.R;

import static com.connectwise.android.utils.LogUtils.LOGW;

/**
 * Common functionality for displaying alert dialogs.
 */
public class DialogUtils {

    /**
     * Applies a color filter to a Drawable.
     *
     * @return a mutated version of the given drawable with a color filter
     * applied.
     */
    public static Drawable convertToGrayScale(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        return applyColorFilter(drawable, Color.LTGRAY);
    }

    /**
     * Applies a specified color filter to a Drawable.
     *
     * @param drawable drawable to filter.
     * @param color    color to apply.
     * @return a mutated Drawable.
     */
    private static Drawable applyColorFilter(Drawable drawable, int color) {
        Drawable mutate = drawable.mutate();
        mutate.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return mutate;
    }

    /**
     * @param context
     * @param pixels
     * @return
     */
    public static int getDensityPixelValue(Context context, int pixels) {
        final float scale = context.getResources().getDisplayMetrics().density;
        //System.out.println((int) (pixels * scale + 0.5f));  Old method
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixels, context.getResources().getDisplayMetrics());
    }

    /**
     * Attempts to hide the soft keyboard if it is visible.
     */
    public static void hideKeyboard(View view) {
        // Hide keyboard
        try {
            InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (NullPointerException ex) {
            LOGW(LogUtils.getTag(view.getClass()), view.getContext().getString(R.string.error_ui_inputmanagernull), ex);
        }
    }


}
