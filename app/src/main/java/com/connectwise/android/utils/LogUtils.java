package com.connectwise.android.utils;

import android.util.Log;

import com.connectwise.android.BuildConfig;

/**
 * Utility class to abstract common logging functionality.
 */
public class LogUtils {

    private static final String TAG_PREFIX = "CWM_";
    private static final int TAG_PREFIX_LENGTH = TAG_PREFIX.length();
    private static final int MAX_TAG_LENGTH = 23;

    /**
     * Convenience methods for dealing with logging.
     */
    private LogUtils() {
    }

    /**
     * Assemble a logging tag from a provided string.
     * The provided value is prefixed with an application and then truncated
     * to 23 characters.
     *
     * @param string value to base the logging tag on.
     * @return a formatted tag value.
     */
    public static String getTag(final String string) {
        if (string.length() > MAX_TAG_LENGTH - TAG_PREFIX_LENGTH) {
            return TAG_PREFIX + string.substring(0, MAX_TAG_LENGTH - TAG_PREFIX_LENGTH - 1);
        }
        return TAG_PREFIX + string;
    }

    /**
     * Assemble a logging tag from the simple name of a provided class.
     * The provided value is prefixed with an application and then truncated
     * to 23 characters.
     * <p/>
     * This method should never be used with class obfuscation functionality.
     *
     * @param clazz class definition to base the logging tag on.
     * @return a formatted tag value.
     */
    public static String getTag(final Class clazz) {
        return getTag(clazz.getSimpleName());
    }

    /**
     * Send a DEBUG log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     */
    public static void LOGD(final String tag, String message) {
        if (BuildConfig.DEBUG || Log.isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, message);
        }
    }

    /**
     * Send a DEBUG log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     * @param cause   An exception to log.
     */
    public static void LOGD(final String tag, String message, Throwable cause) {
        if (BuildConfig.DEBUG || Log.isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, message, cause);
        }
    }

    /**
     * Send a VERBOSE log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     */
    public static void LOGV(final String tag, String message) {
        if (BuildConfig.DEBUG && Log.isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, message);
        }
    }

    /**
     * Send a VERBOSE log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     * @param cause   An exception to log.
     */
    public static void LOGV(final String tag, String message, Throwable cause) {
        if (BuildConfig.DEBUG && Log.isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, message, cause);
        }
    }

    /**
     * Send a INFO log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     */
    public static void LOGI(final String tag, String message) {
        Log.i(tag, message);
    }

    /**
     * Send a INFO log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     * @param cause   An exception to log.
     */
    public static void LOGI(final String tag, String message, Throwable cause) {
        Log.i(tag, message, cause);
    }

    /**
     * Send a WARN log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     */
    public static void LOGW(final String tag, String message) {
        Log.w(tag, message);
    }

    /**
     * Send a WARN log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     * @param cause   An exception to log.
     */
    public static void LOGW(final String tag, String message, Throwable cause) {
        Log.w(tag, message, cause);
    }

    /**
     * Send a ERROR log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     */
    public static void LOGE(final String tag, String message) {
        Log.e(tag, message);
    }

    /**
     * Send a ERROR log message.
     *
     * @param tag     Used to identify the source of a log message. It usually identifies the class or activity where the log call occurs.
     * @param message The message to log.
     * @param cause   An exception to log.
     */
    public static void LOGE(final String tag, String message, Throwable cause) {
        Log.e(tag, message, cause);
    }

}