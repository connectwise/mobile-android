package com.connectwise.android.utils;

import java.util.Calendar;

/**
 * Utility methods for manipulating date and time values.
 */
public class DateUtils {

    /**
     * Utility method to retrieve the current calendar year
     *
     * @return integer value of the current year.
     */
    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

}
