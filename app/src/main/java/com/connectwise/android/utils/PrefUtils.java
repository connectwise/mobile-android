package com.connectwise.android.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.connectwise.android.model.Credential;

/**
 * Convenience methods for dealing with locally stored preferences.
 */
public class PrefUtils {

    // Preferences to identify each stage of the setup process
    public static final String PREF_SETUP_STAGE_EULA = "pref_setup_stage_eula";
    public static final String PREF_SETUP_STAGE_CRED = "pref_setup_stage_cred";
    public static final String PREF_SETUP_STAGE_SYNC = "pref_setup_stage_sync";
    // Identifier to unify all preferences
    private static final String PREF_FILE = "ConnectWise_Mobile";
    private static final String PREF_SETUP_COMPLETE = "pref_setup_complete";
    private static final String PREF_CREDS_URL = "pref_creds_url";
    private static final String PREF_CREDS_COMPANY = "pref_creds_company";
    private static final String PREF_CREDS_MEMBER = "pref_creds_member";
    private static final String PREF_CREDS_PASSWORD = "pref_creds_password";
    private static final String PREF_CREDS_USESSL = "pref_creds_usessl";

    /**
     * Returns a value indicating if a specified initial setup stage has been completed.
     *
     * @param context preference context to search
     * @param stage   specified setup stage
     * @return a boolean value indicating completion of the specified stage
     */
    public static boolean isStageComplete(final Context context, final String stage) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        return pref.getBoolean(stage, false);
    }

    /**
     * Sets a preference value indicating an initial setup stage has been completed.
     *
     * @param context  preference context to search
     * @param stage    specified setup stage
     * @param complete boolean indicating the state of the stage
     */
    public static void markStageComplete(Context context, String stage, Boolean complete) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        pref.edit().putBoolean(stage, complete).apply();
    }

    /**
     * Returns a value indicating if the entire initial setup has been completed.
     *
     * @param context preference context to search
     * @return boolean value indicating if the initial setup is completed
     */
    public static boolean isSetupComplete(final Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        return pref.getBoolean(PREF_SETUP_COMPLETE, false);
    }

    /**
     * Sets a preference value indicating the initial setup has been completed.
     *
     * @param context  preference context to search
     * @param complete boolean value indicating the state of the initial setup
     */
    public static void markSetupComplete(Context context, Boolean complete) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        pref.edit().putBoolean(PREF_SETUP_COMPLETE, complete).apply();
    }

    /**
     * Mechanism for retrieving a stored credential.
     *
     * @param context preference context to search
     * @return a stored Credential model object
     */
    public static Credential getCredential(final Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        return new Credential(
                pref.getString(PREF_CREDS_URL, ""),
                pref.getString(PREF_CREDS_COMPANY, ""),
                pref.getString(PREF_CREDS_MEMBER, ""),
                pref.getString(PREF_CREDS_PASSWORD, ""),
                pref.getBoolean(PREF_CREDS_USESSL, false));
    }

    /**
     * Mechanism for persisting authentication credentials as a preference.
     *
     * @param context preference context to search
     * @param cred    Credential model to be stored
     */
    public static void setCredential(Context context, Credential cred) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE).edit();
        editor.putString(PREF_CREDS_URL, cred.url);
        editor.putString(PREF_CREDS_COMPANY, cred.companyId);
        editor.putString(PREF_CREDS_MEMBER, cred.memberId);
        editor.putString(PREF_CREDS_PASSWORD, cred.password);
        editor.putBoolean(PREF_CREDS_USESSL, cred.useSSL);
        editor.apply();
    }

    /**
     * Removes saved credentials from shared preferences.
     * <p>This will also reset the credential, sync and initial setup completion to false.
     *
     * @param context preference context to search
     */
    public static void deleteCredential(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE).edit();
        editor.remove(PREF_CREDS_URL);
        editor.remove(PREF_CREDS_COMPANY);
        editor.remove(PREF_CREDS_MEMBER);
        editor.remove(PREF_CREDS_PASSWORD);
        editor.remove(PREF_CREDS_USESSL);
        markStageComplete(context, PREF_SETUP_STAGE_CRED, false);
        markStageComplete(context, PREF_SETUP_STAGE_SYNC, false);
        markSetupComplete(context, false);
        editor.apply();
    }

}
