package com.connectwise.android.model.contacts;

import com.connectwise.android.model.Model;
import com.connectwise.android.model.companies.Company;

/**
 * Lightweight data representation of a company contact.
 * <p/>
 * <p>This is used primarily for quick display within ListView UI components.</p>
 */
public class Contact extends Model {

    public String firstName;
    public String lastName;
    public String phoneNumber;
    public Company company = new Company();
    public String title;

    /**
     * @return
     */
    @Override
    public ContactId getRecordId() {
        return new ContactId(mRecordId);
    }

    /**
     * All Model extensions should return a basic String representation of themselves.
     *
     * @return string representation of a model.
     */
    @Override
    public String toString() {
        return ContactFormatter.toFormattedString(this, ContactFormatter.FIRSTLAST);
    }

}
