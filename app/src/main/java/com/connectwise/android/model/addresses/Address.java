package com.connectwise.android.model.addresses;

import com.connectwise.android.model.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Data representation of an address.
 */
public class Address extends Model {

    public List<String> streetLines = new ArrayList<>();
    public String city;
    public String state;
    public String zip;
    public String country;

    /**
     * @return
     */
    @Override
    public AddressId getRecordId() {
        return new AddressId(mRecordId);
    }

    /**
     * Default string representation.
     *
     * @return string representation of a model.
     */
    @Override
    public String toString() {
        return AddressFormatter.toFormattedString(this, AddressFormatter.FULL);
    }
}
