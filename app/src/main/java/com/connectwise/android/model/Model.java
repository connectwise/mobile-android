package com.connectwise.android.model;

/**
 * Base data representation of model objects with record ids.
 */
public abstract class Model {

    /**
     * Object record id.
     */
    protected long mRecordId;

    /**
     * @return
     */
    public abstract RecordId getRecordId();

    /**
     * @param recordId
     */
    public void setRecordId(long recordId) {
        mRecordId = recordId;
    }

    /**
     * All Model extensions should return a basic String representation of themselves.
     *
     * @return string representation of a model.
     */
    @Override
    public abstract String toString();

}
