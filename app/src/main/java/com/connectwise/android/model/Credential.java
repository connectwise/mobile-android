package com.connectwise.android.model;

/**
 * Model object to encapsulate a members credentials for performing server operations.
 */
public class Credential {

    public String url = null;
    public String companyId = null;
    public String memberId = null;
    public String password = null;
    public boolean useSSL = false;

    /**
     * Parametrized constructor for building a Credential in one method call.
     *
     * @param url       server URL location
     * @param companyId company tenant
     * @param memberId  username of the credential member
     * @param password  password fo the credential member
     * @param useSSL    boolean value indicating if SSL should be used during connections
     */
    public Credential(String url, String companyId, String memberId, String password, boolean useSSL) {
        this.url = url;
        this.companyId = companyId;
        this.memberId = memberId;
        this.password = password;
        this.useSSL = useSSL;
    }

}
