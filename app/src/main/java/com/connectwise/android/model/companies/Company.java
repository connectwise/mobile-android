package com.connectwise.android.model.companies;

import com.connectwise.android.model.Model;
import com.connectwise.android.model.addresses.CompanyAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Lightweight data representation of a company.
 * <p/>
 * <p>This is used primarily for quick display within ListView UI components.</p>
 */
public class Company extends Model {

    public String id;
    public String name;
    public List<CompanyAddress> addresses = new ArrayList<>();
    public String phoneNumber;
    public String territory;
    public String type;
    public String status;

    /**
     * @return
     */
    @Override
    public CompanyId getRecordId() {
        return new CompanyId(mRecordId);
    }

    /**
     * Default string representation fo a Company.
     *
     * @return string indicating the company name.
     */
    @Override
    public String toString() {
        return name;
    }

}
