package com.connectwise.android.model.addresses;

/**
 * Data representation of a address at a company.
 */
public class CompanyAddress extends Address {

    public boolean defaultFlag;
    public String siteName;
    public boolean inactiveFlag;

}
