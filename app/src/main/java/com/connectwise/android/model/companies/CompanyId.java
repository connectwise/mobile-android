package com.connectwise.android.model.companies;

import android.os.Parcel;
import android.os.Parcelable;

import com.connectwise.android.model.RecordId;

/**
 *
 */
public class CompanyId extends RecordId {

    /**
     * @param recordId
     */
    public CompanyId(long recordId) {
        super(recordId);
    }

    /**
     * @param in
     */
    protected CompanyId(Parcel in) {
        super(in);
    }

    /**
     * @return
     */
    @Override
    public int getType() {
        return COMPANY;
    }

    /**
     *
     */
    public static final Parcelable.Creator<CompanyId> CREATOR = new Parcelable.Creator<CompanyId>() {
        public CompanyId createFromParcel(Parcel in) {
            return new CompanyId(in);
        }

        public CompanyId[] newArray(int size) {
            return new CompanyId[size];
        }
    };

}

