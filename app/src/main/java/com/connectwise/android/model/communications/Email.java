package com.connectwise.android.model.communications;

/**
 * Data representation of an email type CommunicationItem.
 */
public class Email extends CommunicationItem {

    /**
     * Default constructor to initialize variables.
     */
    public Email() {
        type = CommunicationType.EMAIL;
    }

}
