package com.connectwise.android.model.communications;

/**
 * Base representation of all communication types.
 */
public abstract class CommunicationItem {

    public int id;
    public String type;
    public String description;
    public String value;
    public boolean isDefaultForType;

    /**
     * Internal representation of the communication types.
     */
    protected static class CommunicationType {
        public static String UNKNOWN = "Unknown";
        public static String EMAIL = "EmailAddress";
        public static String PHONE = "PhoneNumber";
        public static String FAX = "FaxNumber";
    }

}
