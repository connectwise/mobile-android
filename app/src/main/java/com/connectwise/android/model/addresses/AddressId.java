package com.connectwise.android.model.addresses;

import android.os.Parcel;

import com.connectwise.android.model.RecordId;

/**
 *
 */
public class AddressId extends RecordId {

    /**
     * @param recordId
     */
    public AddressId(long recordId) {
        super(recordId);
    }

    /**
     * @return
     */
    @Override
    public int getType() {
        return ADDRESS;
    }

    /**
     * @param in
     */
    protected AddressId(Parcel in) {
        super(in);
    }

}
