package com.connectwise.android.model.companies;

import java.io.Serializable;
import java.util.Date;

/**
 * Detailed data representation of a company.
 * <p/>
 * <p>This is suitable for full screen display and edit views.</p>
 */
public class CompanyDetail extends Company implements Serializable {

    public String faxNumber;
    public String webSite;
    public int defaultContactRecId;
    public int defaultBillingContactRecId;
    public Date lastUpdate;

}
