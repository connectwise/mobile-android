package com.connectwise.android.model.communications;

/**
 * Data representation of a fax type CommunicationItem.
 */
public class Fax extends CommunicationItem {

    /**
     * Default constructor to initialize variables.
     */
    public Fax() {
        type = CommunicationType.FAX;
    }

}
