package com.connectwise.android.model.addresses;

import android.text.TextUtils;

/**
 * Static String formatter for Address Model objects.
 */
public class AddressFormatter {

    public static final int FULL = 0;
    public static final int CITY_STATE = 1;
    private static final String DELIMITER = ", ";

    /**
     * Formats an Address object with a specified format.
     *
     * @param format int value determining which format to return.
     * @return Address data formatted as a String.
     */
    public static String toFormattedString(Address address, int format) {
        StringBuilder builder = new StringBuilder();

        if (format == FULL) {
            if (address.streetLines.size() > 0) {
                for (String streetLine : address.streetLines) {
                    append(builder, streetLine);
                }
            }
            append(builder, address.city);
            append(builder, address.state);
            append(builder, address.zip);
            append(builder, address.country);
        } else if (format == CITY_STATE) {
            append(builder, address.city);
            append(builder, address.state);
        }
        return builder.toString();
    }

    /**
     * Appends a String value into the StringBuilder if the value is not empty.
     * </p>
     * <p>
     * If the builder already has data in it a comma is also added before the new value.
     * </p>
     *
     * @param builder StringBuilder to append to.
     * @param value   String value to append if it is not empty.
     */
    private static void append(StringBuilder builder, String value) {
        if (!TextUtils.isEmpty(value)) {
            if (builder.length() > 0) {
                builder.append(DELIMITER);
            }
            builder.append(value);
        }
    }

}
