package com.connectwise.android.model.communications;

/**
 * Data representation of a phone type CommunicationItem.
 */
public class Phone extends CommunicationItem {

    /**
     * Default constructor to initialize variables.
     */
    public Phone() {
        type = CommunicationType.PHONE;
    }

}
