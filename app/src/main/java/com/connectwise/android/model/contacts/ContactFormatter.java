package com.connectwise.android.model.contacts;

/**
 * Static string formatter for Contact Model objects.
 */
public class ContactFormatter {

    public static int FIRSTLAST = 0;
    private static final String FORWARD = "%1$s %2$s";

    /**
     * Formats a Contact object with a specified format.
     *
     * @param format int value determining which format to return.
     * @return Contact data formatted as a String.
     */
    public static String toFormattedString(Contact contact, int format) {
        // Currently only one format, FIRST LAST is required.
        return String.format(FORWARD, contact.firstName, contact.lastName);
    }

}
