package com.connectwise.android.model.contacts;

import android.os.Parcel;
import android.os.Parcelable;

import com.connectwise.android.model.RecordId;

/**
 *
 */
public class ContactId extends RecordId {

    /**
     * @param recordId
     */
    public ContactId(long recordId) {
        super(recordId);
    }

    /**
     * @param in
     */
    protected ContactId(Parcel in) {
        super(in);
    }

    @Override
    public int getType() {
        return CONTACT;
    }

    /**
     *
     */
    public static final Parcelable.Creator<ContactId> CREATOR = new Parcelable.Creator<ContactId>() {
        public ContactId createFromParcel(Parcel in) {
            return new ContactId(in);
        }

        public ContactId[] newArray(int size) {
            return new ContactId[size];
        }
    };

}
