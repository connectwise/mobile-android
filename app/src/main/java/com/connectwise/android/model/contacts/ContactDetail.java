package com.connectwise.android.model.contacts;

import com.connectwise.android.model.addresses.Address;
import com.connectwise.android.model.communications.Email;
import com.connectwise.android.model.communications.Fax;
import com.connectwise.android.model.communications.Phone;

import java.util.ArrayList;
import java.util.List;

/**
 * Detailed data representation of a contact.
 * <p/>
 * <p>This is suitable for full screen display and edit views.</p>
 */
public class ContactDetail extends Contact {

    public List<Phone> phones;
    public List<Email> emails;
    public List<Fax> faxes;
    public Address personalAddress;
    public Address companyAddress;

    /**
     * Default constructor to initialize variables.
     */
    public ContactDetail() {
        phones = new ArrayList<>();
        emails = new ArrayList<>();
        faxes = new ArrayList<>();
    }

}
