package com.connectwise.android.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 */
public class RecordId implements Parcelable {

    public static int COMPANY = 0;
    public static int CONTACT = 1;
    public static int TICKET = 2;
    public static int ADDRESS = 3;

    /**
     *
     */
    public long id;

    /**
     * @param id
     */
    public RecordId(long id) {
        this.id = id;
    }

    /**
     * @param in
     */
    protected RecordId(Parcel in) {
        id = in.readInt();
    }

    /**
     * @return
     */
    public int getType() {
        return -1;
    }

    /**
     * @return
     */
    public int describeContents() {
        return 0;
    }

    /**
     * @param out
     * @param flags
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(id);
    }

    /**
     *
     */
    public static final Parcelable.Creator<RecordId> CREATOR = new Parcelable.Creator<RecordId>() {
        public RecordId createFromParcel(Parcel in) {
            return new RecordId(in);
        }

        public RecordId[] newArray(int size) {
            return new RecordId[size];
        }
    };

}