package com.connectwise.android.data.contacts.parsers;

import android.text.TextUtils;

import com.connectwise.android.data.addresses.AddressDataFields;
import com.connectwise.android.data.contacts.ContactDataFields;
import com.connectwise.android.data.parsers.BaseXmlPullParser;
import com.connectwise.android.model.addresses.CompanyAddress;
import com.connectwise.android.model.contacts.Contact;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser to assemble Contact objects.
 */
public class ContactXmlPullParser extends BaseXmlPullParser<Contact> {

    private List<Contact> mContactList = new ArrayList<>();

    /**
     * Iterates through an InputStream to assemble Contact objects.
     *
     * @param parser already open parser containing data.
     * @return a populated list of Contact objects, or an empty list if the stream contained no data.
     * @throws XmlPullParserException thrown when the parsing code is incorrectly configured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    @Override
    public List<Contact> parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parseList(parser);
        return mContactList;
    }

    /**
     * Parses an xml document to map data to Contact objects.
     *
     * @param parser XmlPullParser populated with data.
     * @throws XmlPullParserException thrown when the parsing code is incorrectly configured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    private void parseList(XmlPullParser parser) throws XmlPullParserException, IOException {

        // Iterate through tags.
        while (parser.next() != XmlPullParser.END_TAG) {
            // Only evaluate beginning tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            switch (parser.getName()) {
                case ContactDataFields.OBJECT_TAG:
                    // Evaluate individual contact tag and add to list.
                    mContactList.add(parseObject(parser));
                    break;
                case ContactDataFields.COLLECTION_TAG:
                    // This is necessary so the Contacts tag is not skipped.
                    // Collection options should be secondary in the switch as it will only be hit once.
                    continue;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }
    }

    /**
     * Parses a Contact tag to return a populated Contact object.
     * </p>
     * <p>
     * This requires that a contact tag exists at the current tag position.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated Contact object.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws IOException            thrown when the parser has an issue moving to the next tag.
     */
    private Contact parseObject(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NAMESPACE, ContactDataFields.OBJECT_TAG);

        Contact contact = new Contact();
        // Lightweight Company objects only contain a single address the default one.
        CompanyAddress defaultAddress = new CompanyAddress();
        defaultAddress.defaultFlag = true;

        while (parser.next() != XmlPullParser.END_TAG) {
            // Continue until the next starting tag
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Handle Company data entry tags.
            switch (parser.getName()) {
                case ContactDataFields.REC_ID:
                    contact.setRecordId(parseLong(parser, ContactDataFields.REC_ID));
                    break;
                case ContactDataFields.FIRST_NAME:
                    contact.firstName = parseString(parser, ContactDataFields.FIRST_NAME);
                    break;
                case ContactDataFields.LAST_NAME:
                    contact.lastName = parseString(parser, ContactDataFields.LAST_NAME);
                    break;
                case AddressDataFields.ADDRESS1:
                    // This is necessary since the API can return empty address entries.
                    String line = parseString(parser, AddressDataFields.ADDRESS1);
                    if (!TextUtils.isEmpty(line)) {
                        defaultAddress.streetLines.add(line);
                    }
                    break;
                case AddressDataFields.ADDRESS2:
                    defaultAddress.streetLines.add(parseString(parser, AddressDataFields.ADDRESS2));
                    break;
                case AddressDataFields.CITY:
                    defaultAddress.city = parseString(parser, AddressDataFields.CITY);
                    break;
                case AddressDataFields.STATE:
                    defaultAddress.state = parseString(parser, AddressDataFields.STATE);
                    break;
                case AddressDataFields.ZIP:
                    defaultAddress.zip = parseString(parser, AddressDataFields.ZIP);
                    break;
                case ContactDataFields.PHONE_NUMBER:
                    contact.phoneNumber = parseString(parser, ContactDataFields.PHONE_NUMBER);
                    break;
                case ContactDataFields.COMPANY_NAME:
                    contact.company.name = parseString(parser, ContactDataFields.COMPANY_NAME);
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

        // Assemble contact and add to return.
        contact.company.addresses.add(defaultAddress);
        return contact;

    }

}

