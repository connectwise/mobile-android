package com.connectwise.android.data.loaders;

/**
 * Wraps the return from an AsyncTaskLoader to determine if an error condition exists.
 * </p>
 * <p>
 * This is necessary since exceptions should never be thrown between the UI and background threads.
 * </p>
 */
public class AsyncTaskLoaderResult<T> {

    public final T result;
    public final Throwable exception;

    /**
     * Creats a result wrapping a return value.
     *
     * @param result return value to add to result.
     */
    public AsyncTaskLoaderResult(T result) {
        this.result = result;
        this.exception = null;
    }

    /**
     * Creates a result from an exception condition.
     *
     * @param exception error details.
     */
    public AsyncTaskLoaderResult(Exception exception) {
        this.result = null;
        this.exception = exception;
    }

    /**
     * Gets error condition.
     *
     * @return true if an error has occurred, else false.
     */
    public boolean isError() {
        return exception != null;
    }

}
