package com.connectwise.android.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ApiCriteria {

    protected static final String ENTRY_XML = "<%1$s>%2$s</%3$s>";
    protected static final String CONDITION_XML = "<Conditions>%s</Conditions>";
    protected static final String AND = "and";
    protected static final String WILDCARD = "*";
    protected static final String EQUALS_STATEMENT = "%1$s = \"%2$s\"";
    protected static final String EQUALS_STATEMENT_NUMERIC = "%1$s = %2$s";
    protected static final String LIKE_STATEMENT = "%1$s like \"%2$s\"";
    protected static final String AND_STATEMENT = " %1$s ";

    // Key Prefixes
    protected static final String EQUALS_KEY = "eq_";
    private HashMap<String, String> mFieldsMap = new HashMap<>();

    /**
     * Add a simple equality criteria.
     * </p>
     * <p>
     * If the data contains a wildcard value, i.e. * the equality automatically becomes a LIKE.
     * </p>
     *
     * @param field field name
     * @param data  data that the field name must contain.
     */
    public void addEquality(String field, String data) {
        mFieldsMap.put(EQUALS_KEY + field, data.contains(WILDCARD) ?
                String.format(LIKE_STATEMENT, field.trim(), data.trim()) :
                String.format(EQUALS_STATEMENT, field.trim(), data.trim()));
    }

    /**
     * Add a simple numeric equality criteria.
     *
     * @param field field name
     * @param data  data that the field name must contain.
     */
    public void addEquality(String field, long data) {
        mFieldsMap.put(EQUALS_KEY + field, String.format(EQUALS_STATEMENT_NUMERIC, field.trim(), Long.toString(data)));
    }

    /**
     * Add a field data set to the conditions map.
     * </p>
     * <p>
     * This data will simply be rendered as a containing XML tag in the condition output.
     * </p>
     *
     * @param field field name
     * @param data  data that the field name must contain.
     */
    public void addNonConditional(String field, String data) {
        mFieldsMap.put(field, data);
    }

    /**
     * Retrieve the XML representation of an API condition statement.
     *
     * @return String representing the condition statement.
     */
    public String getXml() {
        if (mFieldsMap.size() == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        ArrayList<Map.Entry<String, String>> entries = new ArrayList<>(mFieldsMap.entrySet());
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<String, String> entry = entries.get(i);
            builder.append(String.format(ENTRY_XML, entry.getKey(), entry.getValue(), entry.getKey()));
        }
        return builder.toString();
    }

    /**
     * Retrieve the XML representation of an API condition statement.
     * </p>
     * <p>
     * This condition statement is formatted similar to a SQL Where clause condition
     * surrounded by a set of <Condition> tags.
     * </p>
     *
     * @return String representing the condition statement.
     */
    public String getConditionXml() {
        if (mFieldsMap.size() == 0) {
            return "";
        }
        ArrayList<String> values = new ArrayList<>(mFieldsMap.values());
        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < values.size(); x++) {
            if (x > 0) {
                builder.append(String.format(AND_STATEMENT, AND));
            }
            builder.append(values.get(x));
        }
        return String.format(CONDITION_XML, builder.toString());
    }

    /**
     * Remove all criteria values.
     */
    public void clear() {
        mFieldsMap.clear();
    }

}
