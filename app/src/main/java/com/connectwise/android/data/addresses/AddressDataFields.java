package com.connectwise.android.data.addresses;

/**
 * Static values for lightweight Address data field names.
 * </p>
 * <p>
 * This is used for parsing address data in ListView visualizations.
 * </p>
 */
public class AddressDataFields {

    public static final String ADDRESS1 = "AddressLine1";
    public static final String ADDRESS2 = "AddressLine2";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String ZIP = "Zip";


}
