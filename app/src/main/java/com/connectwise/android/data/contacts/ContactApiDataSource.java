package com.connectwise.android.data.contacts;

import android.content.Context;

import com.connectwise.android.data.ApiDataSource;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.contacts.parsers.ContactXmlPullParser;
import com.connectwise.android.model.Credential;
import com.connectwise.android.model.contacts.Contact;

import java.util.List;

/**
 * DataSource for performing interactions with Contact data on the ConnectWise server.
 */
public class ContactApiDataSource extends ApiDataSource<Contact> {

    private static final String mFindRequest = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
            "<FindPartnerContactsAction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
            "%s" + // Integration XML
            "%s" + // Conditions
            "<OrderBy>FirstName</OrderBy>" +
            "</FindPartnerContactsAction>";
    protected static String TAG = "ContactApiDataSource";

    /**
     * Constructor which stores class wide values.
     *
     * @param credential server security credentials.
     */
    public ContactApiDataSource(Context context, Credential credential) {
        super(context, credential);
    }

    /**
     * Performs an API call to the ConnectWise server to retrieve a list of Contact objects for a set of criteria.
     *
     * @return a populated list of Contact objects or an empty list if criteria does not produce values.
     */
    public List<Contact> getContactList(String criteria) throws DataAccessException {
        String action = String.format(mFindRequest, getIntegrationXml(), criteria);
        return sendRequest(action, new ContactXmlPullParser());
    }

}
