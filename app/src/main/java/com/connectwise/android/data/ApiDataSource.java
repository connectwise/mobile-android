package com.connectwise.android.data;

import android.content.Context;

import com.connectwise.android.R;
import com.connectwise.android.data.parsers.InputStreamParser;
import com.connectwise.android.model.Credential;
import com.connectwise.android.utils.HttpUtils;
import com.connectwise.android.utils.LogUtils;
import com.connectwise.android.utils.NetworkUtils;
import com.squareup.okhttp.OkHttpClient;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Base DataSource for connecting to the ConnectWise API.
 */
public abstract class ApiDataSource<T> {

    // TODO Use credentials.
    protected static final String BASE_URL = "%s://%s/%s/services/system_io/integration_io/processclientaction.rails";
    protected static final String INT_XML = "<CompanyName>%s</CompanyName><IntegrationLoginId>%s</IntegrationLoginId><IntegrationPassword>%s</IntegrationPassword>";
    protected static final String ACTION_PREFIX = "actionString=%s";
    protected static final String HTTP_METHOD = "POST";
    protected static final String INTU = "intadmin";
    protected static final String INTP = "intadmin";
    protected static String TAG = "ApiDataSource<T>";
    protected static Credential mCredential;
    protected String mProtocol;
    protected String mServer;
    protected String mServerVersion = "v4_6_release";
    private OkHttpClient mClient = new OkHttpClient();
    private Context mContext;

    /**
     * Base constructor to store common values.
     *
     * @param context    Application context to use.
     * @param credential Credential.
     */
    protected ApiDataSource(Context context, Credential credential) {
        mContext = context;
        mCredential = credential;
        mProtocol = credential.useSSL ? HttpUtils.HTTPS : HttpUtils.HTTP;
        mServer = credential.url;
    }

    /**
     * Assemble integration credentials into XML for sending to the ConnectWise server.
     *
     * @return String values containing the integration credentials.
     */
    protected static String getIntegrationXml() {
        return String.format(INT_XML, mCredential.companyId, INTU, INTP);
    }

    /**
     * Assemble a server URL.
     *
     * @return String value representing the ConnectWise server URL.
     */
    protected String getBaseUrl() {
        return String.format(BASE_URL, mProtocol, mServer, mServerVersion);
    }

    /**
     * Executes a request to the API server.
     *
     * @param action String representing the action to perform on the API server.
     * @param parser Parser implementation to consume data with.
     * @return
     */
    protected List<T> sendRequest(String action, InputStreamParser<T> parser) throws DataAccessException {

        if (NetworkUtils.isAirplaneModeOn(mContext)) {
            throw new DataAccessException(mContext.getString(R.string.error_data_airplane));
        }

        HttpURLConnection connection = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;

        try {
            // Configure HTTP post.
            connection = mClient.open(new URL(getBaseUrl()));
            connection.setDoOutput(true);
            connection.setRequestMethod(HTTP_METHOD);
            connection.setUseCaches(false);
            // Build actionString for web request from integration data and criteria.
            byte[] data = String.format(ACTION_PREFIX, URLEncoder.encode(action, HttpUtils.Encoding.UTF_8)).getBytes(HttpUtils.Encoding.UTF_8);
            // Post
            outputStream = connection.getOutputStream();
            outputStream.write(data);
            outputStream.flush();
            outputStream.close();
            // Read the response.
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new DataAccessException(String.format(mContext.getText(R.string.error_data_unexpected_http_response).toString(),
                        connection.getResponseCode(),
                        connection.getResponseMessage()));
            }
            // Parse return into model objects.
            inputStream = connection.getInputStream();
            return parser.parse(inputStream);
        } catch (MalformedURLException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_malformedurl).toString(), ex);
        } catch (ProtocolException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_protocol).toString(), ex);
        } catch (UnsupportedEncodingException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_unsupported_encoding).toString(), ex);
        } catch (XmlPullParserException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_xml_parse).toString(), ex);
        } catch (UnknownHostException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_unknownhost).toString(), ex);
        } catch (IOException ex) {
            throw new DataAccessException(mContext.getText(R.string.error_data_ioexception).toString(), ex);
        } finally {
            // Try to cleanup.
            try {
                if (outputStream != null) outputStream.close();
                if (inputStream != null) inputStream.close();
            } catch (IOException ex) {
                LogUtils.LOGE(TAG, mContext.getText(R.string.error_data_cleanup_ioerror).toString(), ex);
            }

        }

    }

}
