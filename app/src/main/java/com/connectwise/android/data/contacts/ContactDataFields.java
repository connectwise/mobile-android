package com.connectwise.android.data.contacts;

/**
 * Static values for lightweight Contact data field names.
 */
public class ContactDataFields {

    public static final String COLLECTION_TAG = "Contacts";
    public static final String OBJECT_TAG = "Contact";
    public static final String REC_ID = "ContactRecID";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String COMPANY_NAME = "CompanyName";
    public static final String PHONE_NUMBER = "Phone";

}
