package com.connectwise.android.data.addresses.parsers;

import com.connectwise.android.data.addresses.CompanyAddressDataFields;
import com.connectwise.android.data.parsers.BaseXmlPullParser;
import com.connectwise.android.model.addresses.CompanyAddress;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser to assemble CompanyAddress objects.
 */
public class CompanyAddressXmlPullParser extends BaseXmlPullParser<CompanyAddress> {

    private List<CompanyAddress> mCompanyAddressList = new ArrayList<>();

    /**
     * Iterates through a XML document to assemble CompanyAddress objects.
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated list of addresses or an empty list of the stream contained none.
     * @throws XmlPullParserException thrown when the parsing code is misconfigured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    @Override
    public List<CompanyAddress> parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parseList(parser);
        return mCompanyAddressList;
    }

    /**
     * Parses an xml document to map data to CompanyDetail objects.
     *
     * @param parser XmlPullParser populated with data.
     * @throws XmlPullParserException thrown when the parsing code is misconfigured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    private void parseList(XmlPullParser parser) throws XmlPullParserException, IOException {

        // Iterate through tags.
        while (parser.next() != XmlPullParser.END_TAG) {
            // Only evaluate beginning tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            switch (parser.getName()) {
                case CompanyAddressDataFields.OBJECT_TAG:
                    // Evaluate individual CompanyAddress tag and add to list.
                    mCompanyAddressList.add(parseObject(parser));
                    break;
                case CompanyAddressDataFields.COLLECTION_TAG:
                    // This is necessary so the Addresses tag is not skipped.
                    // Collection options should be secondary in the switch as it will only be hit once.
                    continue;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

    }

    /**
     * Parses a Company tag to return a populated CompanyDetail object.
     * </p>
     * <p>
     * This requires that a company tag exists at the current tag position.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated CompanyDetail object.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws IOException            thrown when the parser has an issue moving to the next tag.
     */
    private CompanyAddress parseObject(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NAMESPACE, CompanyAddressDataFields.OBJECT_TAG);

        CompanyAddress companyAddress = new CompanyAddress();

        while (parser.next() != XmlPullParser.END_TAG) {
            // Continue until the next starting tag
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Handle CompanyAddress data entry tags.
            switch (parser.getName()) {
                case CompanyAddressDataFields.REC_ID:
                    companyAddress.setRecordId(parseLong(parser, CompanyAddressDataFields.REC_ID));
                    break;
                case CompanyAddressDataFields.SITE_NAME:
                    companyAddress.siteName = parseString(parser, CompanyAddressDataFields.SITE_NAME);
                    break;
                case CompanyAddressDataFields.SITE_DEFAULT:
                    companyAddress.defaultFlag = parseBoolean(parser, CompanyAddressDataFields.SITE_DEFAULT);
                    break;
                case CompanyAddressDataFields.STREET_LINES_COLLECTION_TAG:
                    companyAddress.streetLines.addAll(parseStreetLines(parser));
                    break;
                case CompanyAddressDataFields.CITY:
                    companyAddress.city = parseString(parser, CompanyAddressDataFields.CITY);
                    break;
                case CompanyAddressDataFields.STATE:
                    companyAddress.state = parseString(parser, CompanyAddressDataFields.STATE);
                    break;
                case CompanyAddressDataFields.ZIP:
                    companyAddress.zip = parseString(parser, CompanyAddressDataFields.ZIP);
                    break;
                case CompanyAddressDataFields.COUNTRY:
                    companyAddress.country = parseString(parser, CompanyAddressDataFields.COUNTRY);
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

        return companyAddress;

    }

    /**
     * Parses a StreetLines tag to return a populated list of String objects.
     * </p>
     * <p>
     * This requires that a StreetLines tag exists at the current tag position.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated List<String> object.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws IOException            thrown when the parser has an issue moving to the next tag.
     */
    private List<String> parseStreetLines(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NAMESPACE, CompanyAddressDataFields.STREET_LINES_COLLECTION_TAG);

        List<String> streetLines = new ArrayList<>();

        while (parser.next() != XmlPullParser.END_TAG) {
            // Continue until the next starting tag
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Handle Company data entry tags.
            switch (parser.getName()) {
                case CompanyAddressDataFields.STREET_LINE:
                    streetLines.add(parseString(parser, CompanyAddressDataFields.STREET_LINE));
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

        return streetLines;

    }


}
