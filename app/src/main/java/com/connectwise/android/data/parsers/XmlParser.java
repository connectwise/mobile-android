package com.connectwise.android.data.parsers;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

/**
 * Interface representing the ability to parse an XmlPullParser to produce populated model objects..
 */
public interface XmlParser<T> {

    /**
     * Iterates through an InputStream to assemble T objects.
     *
     * @param parser already open parser containing data.
     * @return a populated T object, on an empty list if the stream contained no data.
     */
    List<T> parse(XmlPullParser parser) throws IOException, XmlPullParserException;

}
