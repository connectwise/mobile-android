package com.connectwise.android.data;

/**
 * Exception representing a catastrophic error during data access.
 */
public class DataAccessException extends Exception {

    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public DataAccessException() {
        super();
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this exception.
     */
    public DataAccessException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this exception.
     * @param throwable
     */
    public DataAccessException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
