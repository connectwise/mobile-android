package com.connectwise.android.data.companies.parsers;

import com.connectwise.android.data.addresses.CompanyAddressDataFields;
import com.connectwise.android.data.addresses.parsers.CompanyAddressXmlPullParser;
import com.connectwise.android.data.companies.CompanyDetailDataFields;
import com.connectwise.android.data.parsers.BaseXmlPullParser;
import com.connectwise.android.model.companies.CompanyDetail;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser to assemble CompanyDetail objects.
 */
public class CompanyDetailXmlPullParser extends BaseXmlPullParser<CompanyDetail> {

    private List<CompanyDetail> mCompanyList = new ArrayList<>();

    /**
     * Iterates through an InputStream to assemble T objects.
     *
     * @param parser already open parser containing data.
     * @return a populated T object, on an empty list if the stream contained no data.
     */
    @Override
    public List<CompanyDetail> parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parseCompanyDetailList(parser);
        return mCompanyList;
    }

    /**
     * Parses an xml document to map data to CompanyDetail objects.
     *
     * @param parser XmlPullParser populated with data.
     * @throws XmlPullParserException thrown when the parsing code is misconfigured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    private void parseCompanyDetailList(XmlPullParser parser) throws XmlPullParserException, IOException {

        // Iterate through tags.
        while (parser.next() != XmlPullParser.END_TAG) {
            // Only evaluate beginning tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            switch (parser.getName()) {
                case CompanyDetailDataFields.OBJECT_TAG:
                    // Evaluate individual company tag and add to list.
                    mCompanyList.add(parseCompanyDetail(parser));
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

    }

    /**
     * Parses a Company tag to return a populated CompanyDetail object.
     * </p>
     * <p>
     * This requires that a company tag exists at the current tag position.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated CompanyDetail object.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws IOException            thrown when the parser has an issue moving to the next tag.
     */
    private CompanyDetail parseCompanyDetail(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NAMESPACE, CompanyDetailDataFields.OBJECT_TAG);

        CompanyDetail companyDetail = new CompanyDetail();

        while (parser.next() != XmlPullParser.END_TAG) {
            // Continue until the next starting tag
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Handle CompanyDetail data entry tags.
            switch (parser.getName()) {
                case CompanyDetailDataFields.REC_ID:
                    companyDetail.setRecordId(parseLong(parser, CompanyDetailDataFields.REC_ID));
                    break;
                case CompanyDetailDataFields.ID:
                    companyDetail.id = parseString(parser, CompanyDetailDataFields.ID);
                    break;
                case CompanyDetailDataFields.NAME:
                    companyDetail.name = parseString(parser, CompanyDetailDataFields.NAME);
                    break;
                case CompanyAddressDataFields.COLLECTION_TAG:
                    CompanyAddressXmlPullParser addressParser = new CompanyAddressXmlPullParser();
                    companyDetail.addresses.addAll(addressParser.parse(parser));
                    break;
                case CompanyDetailDataFields.FAX_NUMBER:
                    companyDetail.faxNumber = parseString(parser, CompanyDetailDataFields.FAX_NUMBER);
                    break;
                case CompanyDetailDataFields.WEBSITE:
                    companyDetail.webSite = parseString(parser, CompanyDetailDataFields.WEBSITE);
                    break;
                case CompanyDetailDataFields.DEFAULT_CONTACT_ID:
                    companyDetail.defaultContactRecId = parseInt(parser, CompanyDetailDataFields.DEFAULT_CONTACT_ID);
                    break;
                case CompanyDetailDataFields.DEFAULT_BILLING_CONTACT_ID:
                    companyDetail.defaultBillingContactRecId = parseInt(parser, CompanyDetailDataFields.DEFAULT_BILLING_CONTACT_ID);
                    break;
                case CompanyDetailDataFields.LAST_UPDATE:
                    companyDetail.lastUpdate = parseDate(parser, CompanyDetailDataFields.LAST_UPDATE);
                    break;
                case CompanyDetailDataFields.PHONE_NUMBER:
                    companyDetail.phoneNumber = parseString(parser, CompanyDetailDataFields.PHONE_NUMBER);
                    break;
                case CompanyDetailDataFields.TERRITORY:
                    companyDetail.territory = parseString(parser, CompanyDetailDataFields.TERRITORY);
                    break;
                case CompanyDetailDataFields.TYPE:
                    companyDetail.type = parseString(parser, CompanyDetailDataFields.TYPE);
                    break;
                case CompanyDetailDataFields.STATUS:
                    companyDetail.status = parseString(parser, CompanyDetailDataFields.STATUS);
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

        return companyDetail;

    }


}
