package com.connectwise.android.data.addresses;

/**
 * Static values for detailed Address data field names.
 * </p>
 * <p>
 * This is used for parsing address data in detailed object visualizations.
 * </p>
 */
public class CompanyAddressDataFields {

    public static final String REC_ID = "Id";
    public static final String COLLECTION_TAG = "Addresses";
    public static final String OBJECT_TAG = "CompanyAddress";
    public static final String SITE_NAME = "SiteName";
    public static final String SITE_DEFAULT = "DefaultFlag";
    public static final String STREET_LINES_COLLECTION_TAG = "StreetLines";
    public static final String STREET_LINE = "string";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String ZIP = "Zip";
    public static final String COUNTRY = "Country";

}
