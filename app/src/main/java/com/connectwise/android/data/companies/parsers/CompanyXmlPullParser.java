package com.connectwise.android.data.companies.parsers;

import android.text.TextUtils;

import com.connectwise.android.data.addresses.AddressDataFields;
import com.connectwise.android.data.companies.CompanyDataFields;
import com.connectwise.android.data.parsers.BaseXmlPullParser;
import com.connectwise.android.model.addresses.CompanyAddress;
import com.connectwise.android.model.companies.Company;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Parser to assemble Company objects.
 */
public class CompanyXmlPullParser extends BaseXmlPullParser<Company> {

    private List<Company> mCompanyList = new ArrayList<>();

    /**
     * Iterates through a XML document to assemble Company objects.
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated list of companies or an empty list of the stream contained none.
     * @throws XmlPullParserException thrown when the parsing code is incorrectly configured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    @Override
    public List<Company> parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parseList(parser);
        return mCompanyList;
    }

    /**
     * Parses an xml document to map data to Company objects.
     *
     * @param parser XmlPullParser populated with data.
     * @throws XmlPullParserException thrown when the parsing code is incorrectly configured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    private void parseList(XmlPullParser parser) throws XmlPullParserException, IOException {

        // Iterate through tags.
        while (parser.next() != XmlPullParser.END_TAG) {
            // Only evaluate beginning tags.
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            switch (parser.getName()) {
                case CompanyDataFields.OBJECT_TAG:
                    // Evaluate individual company tag and add to list.
                    mCompanyList.add(parseObject(parser));
                    break;
                case CompanyDataFields.COLLECTION_TAG:
                    // This is necessary so the Companies tag is not skipped.
                    // Collection options should be secondary in the switch as it will only be hit once.
                    continue;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }
    }

    /**
     * Parses a Company tag to return a populated Company object.
     * </p>
     * <p>
     * This requires that a company tag exists at the current tag position.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @return a populated Company object.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws IOException            thrown when the parser has an issue moving to the next tag.
     */
    private Company parseObject(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, NAMESPACE, CompanyDataFields.OBJECT_TAG);

        Company company = new Company();
        // Lightweight Company objects only contain a single address the default one.
        CompanyAddress defaultAddress = new CompanyAddress();
        defaultAddress.defaultFlag = true;

        while (parser.next() != XmlPullParser.END_TAG) {
            // Continue until the next starting tag
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            // Handle Company data entry tags.
            switch (parser.getName()) {
                case CompanyDataFields.REC_ID:
                    company.setRecordId(parseLong(parser, CompanyDataFields.REC_ID));
                    break;
                case CompanyDataFields.ID:
                    company.id = parseString(parser, CompanyDataFields.ID);
                    break;
                case CompanyDataFields.NAME:
                    company.name = parseString(parser, CompanyDataFields.NAME);
                    break;
                case AddressDataFields.ADDRESS1:
                    // This is necessary since the API can return empty address entries.
                    String line = parseString(parser, AddressDataFields.ADDRESS1);
                    if (!TextUtils.isEmpty(line)) {
                        defaultAddress.streetLines.add(line);
                    }
                    break;
                case AddressDataFields.ADDRESS2:
                    defaultAddress.streetLines.add(parseString(parser, AddressDataFields.ADDRESS2));
                    break;
                case AddressDataFields.CITY:
                    defaultAddress.city = parseString(parser, AddressDataFields.CITY);
                    break;
                case AddressDataFields.STATE:
                    defaultAddress.state = parseString(parser, AddressDataFields.STATE);
                    break;
                case AddressDataFields.ZIP:
                    defaultAddress.zip = parseString(parser, AddressDataFields.ZIP);
                    break;
                case CompanyDataFields.PHONE_NUMBER:
                    company.phoneNumber = parseString(parser, CompanyDataFields.PHONE_NUMBER);
                    break;
                case CompanyDataFields.TERRITORY:
                    company.territory = parseString(parser, CompanyDataFields.TERRITORY);
                    break;
                case CompanyDataFields.TYPE:
                    company.type = parseString(parser, CompanyDataFields.TYPE);
                    break;
                case CompanyDataFields.STATUS:
                    company.status = parseString(parser, CompanyDataFields.STATUS);
                    break;
                default:
                    // Skip any tags that aren't explicitly handled.
                    skip(parser);
                    break;
            }
        }

        // Assemble company and add to return.
        company.addresses.add(defaultAddress);
        return company;

    }

}
