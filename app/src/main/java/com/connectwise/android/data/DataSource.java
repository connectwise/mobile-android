package com.connectwise.android.data;

/**
 * Abstract base class for all data sources.
 */
public abstract class DataSource {

    /**
     * Internal base constructor.
     */
    protected DataSource() {
    }

    /**
     * All data sources must implement a close functionality to safely dispose connections.
     */
    public abstract void close();

}
