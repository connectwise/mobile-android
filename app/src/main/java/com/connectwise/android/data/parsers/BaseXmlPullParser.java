package com.connectwise.android.data.parsers;

import android.text.TextUtils;

import com.connectwise.android.utils.HttpUtils;
import com.connectwise.android.utils.LogUtils;

import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Base XML data parser.
 */
public abstract class BaseXmlPullParser<T> implements InputStreamParser<T>, XmlParser<T> {

    protected static final String NAMESPACE = null;
    protected static final String TAG_OPEN = "<";
    protected static final String TAG_CLOSE = ">";
    private static final String[] DATE_FORMATS = {"", ""}; // TODO Determine date formats.
    private static final String TAG = "BaseXmlPullParser";

    /**
     * Iterates through a XML document to assemble T objects.
     *
     * @param inputStream data stream containing data.
     * @return a populated T object or an empty list if the stream contained none.
     * @throws XmlPullParserException thrown when the parsing code is misconfigured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    @Override
    public List<T> parse(InputStream inputStream) throws XmlPullParserException, IOException {
        return parse(getParserInstance(inputStream));
    }

    /**
     * Iterates through an InputStream to assemble T objects.
     *
     * @param parser already open parser containing data.
     * @return a populated T object, on an empty list if the stream contained no data.
     */
    @Override
    public abstract List<T> parse(XmlPullParser parser) throws IOException, XmlPullParserException;

    /**
     * Initializes a parser from an InputStream.
     *
     * @param inputStream
     * @return an XmlPullParser instance.
     * @throws XmlPullParserException thrown when the parsing code is misconfigured.
     * @throws IOException            thrown when the parser has an issue moving through the tag structure.
     */
    protected XmlPullParser getParserInstance(InputStream inputStream) throws XmlPullParserException, IOException {

        Reader reader = new InputStreamReader(inputStream, HttpUtils.Encoding.UTF_8);
        InputSource inputSource = new InputSource(reader);
        inputSource.setEncoding(HttpUtils.Encoding.UTF_8);
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(reader);
        parser.nextTag();
        return parser;

    }

    /**
     * Generic parse method to retrieve a single integer value enclosed by the current tag.
     * </p>
     * <p>
     * This requires that a single integer value exists in the current tag with no sub-tags
     * nested beneath.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @param tag    tag name.
     * @return integer value.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected int parseInt(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, tag);
        int value = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            value = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, NAMESPACE, tag);
        return value;
    }

    /**
     * Generic parse method to retrieve a single long value enclosed by the current tag.
     * </p>
     * <p>
     * This requires that a single long value exists in the current tag with no sub-tags
     * nested beneath.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @param tag    tag name.
     * @return long value.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected long parseLong(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, tag);
        long value = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            value = Long.parseLong(parser.getText());
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, NAMESPACE, tag);
        return value;
    }

    /**
     * Generic parse method to retrieve a single string value enclosed by the current tag.
     * </p>
     * <p>
     * This requires that a single String value exists in the current tag with no sub-tags
     * nested beneath.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @param tag    tag name.
     * @return String value.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected String parseString(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, tag);
        String value = "";
        if (parser.next() == XmlPullParser.TEXT) {
            value = parser.getText();
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, NAMESPACE, tag);
        return value;
    }

    /**
     * Generic parse method to retrieve a single boolean value enclosed by the current tag.
     * </p>
     * <p>
     * This requires that a single boolean value exists in the current tag with no sub-tags
     * nested beneath.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @param tag    tag name.
     * @return boolean value.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected boolean parseBoolean(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, tag);
        boolean value = false;
        if (parser.next() == XmlPullParser.TEXT) {
            value = Boolean.parseBoolean(parser.getText());
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, NAMESPACE, tag);
        return value;
    }

    /**
     * Generic parse method to retrieve a single Date value enclosed by the current tag.
     * </p>
     * <p>
     * This requires that a single Date value exists in the current tag with no sub-tags
     * nested beneath. This function will attempt to parse the data in multiple formats
     * designated by the DATE_FORMATS String array until
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @param tag    tag name.
     * @return String value.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected Date parseDate(XmlPullParser parser, String tag) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, tag);
        Date value = null;
        if (parser.next() == XmlPullParser.TEXT) {
            String nodeText = parser.getText();
            if (!TextUtils.isEmpty(nodeText)) {
                // Xml node has data, attempt to parse into a date with each format
                for (String DATE_FORMAT : DATE_FORMATS) {
                    try {
                        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                        value = format.parse(nodeText);
                    } catch (ParseException ex) {
                        // Returned XML value did not match attempted format.
                        LogUtils.LOGW(TAG, String.format("Failed to parse date, %1$s, using format, %2$s", value, DATE_FORMAT)); // TODO
                    }
                }
                if (value == null) {
                    // Date retaining its NULL value indicates all parse attempts failed.
                    // Since update date isn't a critical field, log error and continue.
                    LogUtils.LOGE(TAG, "Unable to parse date, %1$s with any available date format. Object parsing will continue leaving this value NULL."); // TODO
                }
            }
            parser.nextTag();
        }
        parser.require(XmlPullParser.END_TAG, NAMESPACE, tag);
        return value;
    }

    /**
     * Skips the entirety of the current tag, including all sub-tags.
     * </p>
     * <p>
     * This functionality tracks the the depth of sub-tags to determine where
     * the top level tag ends.
     * </p>
     *
     * @param parser XmlPullParser populated with data.
     * @throws XmlPullParserException thrown if the current tag position is not the expected starting tag.
     * @throws java.io.IOException    thrown when the parser has an issue moving to the next tag.
     */
    protected void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
