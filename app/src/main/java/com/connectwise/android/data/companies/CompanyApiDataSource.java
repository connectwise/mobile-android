package com.connectwise.android.data.companies;

import android.content.Context;

import com.connectwise.android.data.ApiDataSource;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.companies.parsers.CompanyXmlPullParser;
import com.connectwise.android.model.Credential;
import com.connectwise.android.model.companies.Company;

import java.util.List;

/**
 * DataSource for performing interactions with Company data on the ConnectWise server.
 */
public class CompanyApiDataSource extends ApiDataSource<Company> {

    private static final String mFindRequest = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
            "<FindPartnerCompaniesAction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
            "%s" + // Integration XML
            "%s" + // Conditions
            "<OrderBy>CompanyName</OrderBy>" +
            "</FindPartnerCompaniesAction>";
    protected static String TAG = "CompanyApiDataSource";

    /**
     * Constructor which stores class wide values.
     *
     * @param credential server security credentials.
     */
    public CompanyApiDataSource(Context context, Credential credential) {
        super(context, credential);
    }

    /**
     * Performs an API call to the ConnectWise server to retrieve a list of Company objects for a set of criteria.
     *
     * @return a populated list of Company objects or an empty list if criteria does not produce values.
     */
    public List<Company> getCompanyList(String criteria) throws DataAccessException {
        String action = String.format(mFindRequest, getIntegrationXml(), criteria);
        return sendRequest(action, new CompanyXmlPullParser());
    }

}