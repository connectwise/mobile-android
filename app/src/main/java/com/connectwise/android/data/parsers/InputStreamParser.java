package com.connectwise.android.data.parsers;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Interface representing the ability to parse an InputStream to produce populated model objects..
 */
public interface InputStreamParser<T> {

    /**
     * Iterates through an InputStream to assemble T objects.
     *
     * @param inputStream data stream containing data.
     * @return a populated T object, on an empty list if the stream contained no data.
     */
    List<T> parse(InputStream inputStream) throws IOException, XmlPullParserException;

}
