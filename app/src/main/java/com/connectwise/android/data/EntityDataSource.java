package com.connectwise.android.data;

import java.util.List;

/**
 * Abstract entity data source.
 * <p/>
 * <p>
 * Used to abstract data access to local or remote data stores. Primarily two
 * types of entities one for display in a ListView and detailed ones for full
 * display and manipulation.
 * </p>
 */
public abstract class EntityDataSource<T1, T2> extends DataSource {

    /**
     * Internal base constructor.
     */
    protected EntityDataSource() {
    }

    /**
     * Retrieve a lightweight entity for a specified Id.
     *
     * @param id specified identifier
     * @return lightweight entity
     */
    public abstract T1 get(int id);

    /**
     * Retrieve a lightweight entity for a specified local guid.
     * <p/>
     * <p>
     * This is used to retrieve specific events off the mobile device that do not
     * yet have an identifier since they have not been synced.
     * </p>
     *
     * @param guid specified mobile guid
     * @return entity
     */
    public abstract T1 get(String guid);

    /**
     * Retrieve a fully populated entity for a specified Id.
     *
     * @param id specified identifier
     * @return entity
     */
    public abstract T2 getDetail(int id);

    /**
     * Retrieve a fully populated entity for a specified local guid.
     * <p/>
     * <p>
     * This is used to retrieve specific events off the mobile device that do not
     * yet have an identifier since they have not been synced.
     * </p>
     *
     * @param guid specified mobile guid
     * @return entity detail object
     */
    public abstract T2 getDetail(String guid);

    /**
     * Retrieve a list of lightweight entities.
     *
     * @return List of lightweight entity.
     */
    public abstract List<T1> getList();

    /**
     * Retrieve a list of fully populated entities.
     *
     * @return List of lightweight entity.
     */
    public abstract List<T2> getDetailList();

    /**
     * Persist a detailed entity to the data store.
     *
     * @param entity a populated entity to persist.
     */
    public abstract void save(T2 entity);

    /**
     * Persist a list of detailed entities to the data store.
     * <p/>
     * <p>
     * This is utilized to bulk persist multiple entities.
     * </p>
     *
     * @param list a List of populated entities to persist.
     */
    public abstract void save(List<T2> list);

}
