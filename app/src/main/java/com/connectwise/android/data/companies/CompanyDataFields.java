package com.connectwise.android.data.companies;

/**
 * Static values for lightweight Company data field names.
 */
public class CompanyDataFields {

    public static final String COLLECTION_TAG = "Companies";
    public static final String OBJECT_TAG = "Company";
    public static final String REC_ID = "CompanyRecID";
    public static final String ID = "CompanyID";
    public static final String NAME = "CompanyName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String TERRITORY = "Territory";
    public static final String TYPE = "Type";
    public static final String STATUS = "Status";

}
