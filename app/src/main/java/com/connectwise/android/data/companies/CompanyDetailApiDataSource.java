package com.connectwise.android.data.companies;

import android.content.Context;

import com.connectwise.android.data.ApiDataSource;
import com.connectwise.android.data.DataAccessException;
import com.connectwise.android.data.companies.parsers.CompanyDetailXmlPullParser;
import com.connectwise.android.model.Credential;
import com.connectwise.android.model.companies.CompanyDetail;

import java.util.List;

/**
 * DataSource for performing interactions with CompanyDetail data on the ConnectWise server.
 */
public class CompanyDetailApiDataSource extends ApiDataSource<CompanyDetail> {

    private static final String mGetRequest = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
            "<GetPartnerCompanyAction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
            "%s" + // Integration XML
            "<Id>%s</Id>" + // Condition
            "</GetPartnerCompanyAction>";
    protected static String TAG = "CompanyDetailApiDataSource";

    /**
     * Constructor which stores class wide values.
     *
     * @param credential server security credentials.
     */
    public CompanyDetailApiDataSource(Context context, Credential credential) {
        super(context, credential);
    }

    /**
     * Performs an API call to the ConnectWise server to retrieve a list of CompanyDetail objects for a set of criteria.
     *
     * @return a populated list of CompanyDetail objects or an empty list if criteria does not produce values.
     */
    public List<CompanyDetail> getCompanyDetailList(String criteria) throws DataAccessException {
        String action = String.format(mGetRequest, getIntegrationXml(), criteria);
        return sendRequest(action, new CompanyDetailXmlPullParser());
    }

}
