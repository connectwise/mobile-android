package com.connectwise.android.data.companies;

/**
 * Static values for detailed Company data field names.
 */
public class CompanyDetailDataFields {

    public static final String OBJECT_TAG = "Company";
    public static final String REC_ID = "Id";
    public static final String ID = "CompanyID";
    public static final String NAME = "CompanyName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String TERRITORY = "Territory";
    public static final String TYPE = "Type";
    public static final String STATUS = "Status";
    public static final String FAX_NUMBER = "FaxNumber";
    public static final String WEBSITE = "WebSite";
    public static final String DEFAULT_CONTACT_ID = "DefaultContactId";
    public static final String DEFAULT_BILLING_CONTACT_ID = "DefaultBillingContactId";
    public static final String LAST_UPDATE = "LastUpdate";

}
