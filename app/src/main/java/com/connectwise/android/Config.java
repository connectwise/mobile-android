package com.connectwise.android;

/**
 * Static configuration values used across the whole application.
 */
public class Config {

    public static final String KEY_RECORD_ID = "record_id";

}
